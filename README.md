# ARSR Thesis Evaluation

This repository contains evaluation code (see [evaluation/](evaluation) folder)  
and scripts used to prepare evaluation data (see [scripts/](scripts) folder).  


**Table of Contents**  
- [Script Setup](#script-setup)
	- [VirtualEnv](#virtualenv)
	- [Dependencies](#dependencies)
- [Testing](#testing)
	- Executing test cases for the main evaluation script.
- [Available Data](#available-data)
	- Data used for evaluation.
- [Evaluation Setup](#evaluation-setup)
	- Notes on how to prepare evaluation (db setup and more).
- [Evaluation Steps](#evaluation-steps)
	- How to use evaluation scripts and perform evaluation.

## Script Setup

Running the python scripts of this repository requires **`Python 3.6+`**!  
Furthermore, dependencies listed in `requirements.txt` must be installed (see below).  


### VirtualEnv

We use [virtualenv](https://virtualenv.pypa.io/en/latest/)
for creating a virtual environment to manage dependencies installed with [pip](https://pypi.org/project/pip/).  
One way to install virtualenv is [via pip](https://virtualenv.pypa.io/en/latest/installation.html#via-pip): `pip install virtualenv`.  

After installation, create a virtual environment as follows.  
Move to the location of this repository and open a terminal in this folder.  
Now create the environment with:
```
virtualenv venv -p python3
```

The `-p python3` part may not be required if it is your system's default.  

When using **Windows Powershell**, now [set execution policies](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy)
so that the environment can be started.  
Note that you have to repeat this step after closing and opening a new terminal window.
```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
```

(**Windows**) Start the environment by executing:
```
venv/Scripts/activate
```

(**Linux**) Start the environment by executing:
```
source venv/bin/activate
```

If successful, you should now see `(venv)` as the prefix of each line in the terminal.  


### Dependencies

Make sure to start the virtual environment before executing the following commands (see above).  

To install dependencies, we use pip as follows:
```
pip install -r requirements.txt
```


## Testing

### Example

You can try out an example and see if everything is running as follows.  
Set up an RDF store with the animal example data found in the [ARSR core repository](https://gitlab.com/dlr-dw/arsr/arsr-core) under `data/example/rdf/animal-example-data.ttl`.  
Next, start the Semantic Relatedness (SemRel) Java API (see respective repo. for instructions).  
Start the Suggestion Service API with example api config 3:
```
python main.py api configs/example/api-conf-3.json -cvr
```  
Now run the evaluation script using the example instances:
```
python collect.py -i ../data/example/animal-example-instances.jsonl
```

### Unit Testing

Unit testing is available for the main python script.  
Change current working directly to be the `evaluation/` folder which contains e.g. `collect.py`.  
Now run the following command:  
```
python -m unittest
```

For more details during execution of test, use:
```
python -m unittest --verbose
```



## Available Data

We call this repository `arsr-evaluation` in the following.  

Currently in `arsr-evaluation/scripts/data-extraction/output/`:
- `ontologies.zip`
	- Contains information about ontologies present in rules of Cedar Value Recommender Evaluation
- `rules.zip`
    - Contains i.a. extracted and formatted rules, based on our selection of ontologies.
    - I.e. each rule item contains a value that is a URI of one of these ontologies.
    - Used in evaluation: `rules/ncbi/extracted/final/`

Data used for the main evaluation is mostly available under `arsr-evaluation/data/cedar/`.  
Please note that this data was initially provided by the Cedar Value Recommender Evaluation
and that **their license** ([data/cedar/LICENSE.txt](data/cedar/LICENSE.txt)) applies.  
- `test-instances.zip`
    - Formatted test instances for NCBI dataset (20278).
    - Formatted test instances for EBI dataset (20278).
    - URI-mappings from the Cedar Value Recommender Evaluation.
    - These files are used in the evaluation.
- `ncbi-ont-rules.zip`
    - Rules for the NCBI BioSample dataset, used by the rule extraction script.
- `ebi-ont-rules.zip`
	- Rules for the EBI BioSample dataset, used by the rule extraction script.


## Evaluation Setup

The required data is already prepared and ready to use.  
For information on how it was created, see [data/data-creation.md](data/data-creation.md).

### 1) Prepare Form Instances

Unzip the testing form instances in `data/cedar/test-instances.zip`.  
The folder `data/cedar/test-instances/` should then contain the following files:
- ebi_instances_formatted.jsonl
- mappings_merged.json
- ncbi_instances_formatted.jsonl

*Note: Form instances currently contain **only a single value** per field.**
- Multi-value fields are not yet supported as we don't have appropriate data.  
- The collection script should work (not tested) when "value_type" or "value_label" is a list (multi-value field).
- The analysis script does not support multi-value fields yet, so it won't work.


### 2) Prepare Rules

Unzip the archived rules, located in the [ARSR Core repository](https://gitlab.com/dlr-dw/arsr/arsr-core) under: `/data/evaluation.zip`.  
The folder `arsr-core/data/evaluation/rules/` should then contain the file `extracted-rules_ncbi.jsonl`.  

*(Skip the following if it worked.)*  
If this zip file is missing, unzip `rules.zip` inside the evaluation repository under: `scripts/data-extraction/output/`.  
Then **copy and rename** the following files from it as follows:
- copy: scripts/data-extraction/output/rules/ncbi/extracted/final/extracted-rules_ncbi.jsonl
	- to: arsr-core/data/evaluation/rules/extracted-rules_ncbi.jsonl
- copy: scripts/data-extraction/output/rules/ebi/extracted/final/extracted-rules_ebi.jsonl
	- to: arsr-core/data/evaluation/rules/extracted-rules_ebi.jsonl


### 3) RDF Store Setup

We use `Blazegraph` as RDF store because it is easy to set up and only requires Java to be installed.  
Install Java if not done yet, then [download the `blazegraph.jar` file](https://github.com/blazegraph/database/releases) and start it with:
```
java -server -"Dlog4j2.enableJndi"=false -jar blazegraph.jar
```

If required, change the port as follows (default is 9999):
```
java -server -"Dlog4j2.enableJndi"=false -D"jetty.port"=9998 -jar blazegraph.jar
```


### 4) Prepare RDF Data

First, **download** the selected ontologies (e.g. using `wget` on Linux):
- EFO 3.40.0: https://github.com/EBISPOT/efo/releases/download/v3.40.0/efo.owl
- PATO 2022-02-20: https://github.com/pato-ontology/pato/raw/v2022-02-20/pato.owl
- UBERON 2022-02-21: https://github.com/obophenotype/uberon/raw/v2022-02-21/uberon.owl
- CL 2022-02-16: https://github.com/obophenotype/cell-ontology/raw/v2022-02-16/cl.owl
- BTO 2021-10-26: https://github.com/BRENDA-Enzymes/BTO/raw/v2022-10-26/bto.owl
- NCIT 2022.02d: https://evs.nci.nih.gov/ftp1/NCI_Thesaurus/archive/22.02d_Release/Thesaurus_22.02d.OWL.zip

Next, this data needs to be uploaded to the RDF store.  
Blazegraph provides a REST API and we can use `curl` (on Linux) to upload data to it.  

**Upload each downloaded ontology** to the RDF store using the following command.  
Make sure to adjust the content type and file name accordingly.
```
curl -H 'Content-Type: application/rdf+xml' --upload-file <ontology_file> -X POST 'http://localhost:9999/blazegraph/sparql'
```

Lastly, **upload the additional triples**.  
These triples were extracted from rule URI mappings
to connect terms of used ontologies through `owl:sameAs` relations.  
Find them in this evaluation repository under: `data/rdf/triples.zip`.  
Unzip this file and upload `ncbi-triples.ttl` to Blazegraph like previously but using `'Content-Type: text/turtle'` instead.    


### 4) Prepare Semantic Relatedness API (SemRel)

This program is written in Java and communicates with the RDF store.  
Setup instructions using Maven are available in the [corresponding repository](https://gitlab.com/dlr-dw/arsr/arsr-semantic-relatedness-api).  


### 5) Prepare Suggestion Service (ARSR Core)

The suggestion service is written in Python and provides an endpoint
for the evaluation script to retrieve suggestions for given inputs.  
It communicates with the SemRel-API (4) if the `lds provider` is used.  
Find more information in the [corresponding repository](https://gitlab.com/dlr-dw/arsr/arsr-core).  



## Evaluation Steps

**System Requirements**:
- Using all 6 ontologies and Resim, Blazegraph can use up to 32 GB of RAM (if available).
    - Using less ontologies (especially without NCIT) needs less RAM (just about 8 to 16 GB on the same 128 GB machine).
- At least 2 GB disk space for the graph data stored in `blazegraph.jnl`.
- Some more disk space for output data and LDS Indexes folder (2-4 GB should be enough).


### 1) Start RDF Store and Semantic Relatedness API (Java)

Start the Blazegraph RDF store (as explained in [Evaluation Setup](#evaluation-Setup) step 3):
```
java -server -"Dlog4j2.enableJndi"=false -D"jetty.port"=9999 -jar blazegraph.jar
```

Next, start the semantic relatedness (SemRel) API, e.g.:
```
java -server -Dport=8010 -jar semrel-1.0.0-SNAPSHOT.jar
```

If the API was already used before, consider removing the folder `/target/Indexes/`.  
This is required if changes to the RDF store were made that could affect the results.  
Keeping the folder has the advantage that relatedness computation will be much quicker and memory consumption of Blazegraph lower.  


### 2) Start Suggestion Service API (Python)

If not done yet, unzip `/data/evaluation.zip` in the [ARSR Core repository](https://gitlab.com/dlr-dw/arsr/arsr-core) first.  
The rules should then be available in `arsr-core/data/evaluation/extracted-rules_ncbi.jsonl`.  

Start the API with the following command:
```
python main.py api configs/evaluation/api-ncbi-resim-config.json -cvr
```

Optionally add `-q` for quiet mode to only show log messages like warnings and errors.  
Don't forget the flag `-cvr` so that the endpoint for the CVR reimplementation is started.


### 3) Run Evaluation (collection)

This step will take the longest as it requests and stores all the suggestions.  

Before starting, make sure to check the `constants` first, which will be used by `collect.py` and `analyse.py`.  
- Edit `arsr-evaluation/evaluation/evaluation/constants.py`.
- Check that the paths and endpoints are correctly set.
- If changes to the used ontologies were made, make sure to add missing prefixes to `ONTO_PREFIXES`.

Change current working directory to `arsr-evaluation/evaluation/`,  
make sure that the virtual environment is activated and start the script:  
```
python collect.py -imax 10000
```

The flag `-imax <num>` will set the maximum number of instances.  
This will overwrite the respective value in `constants.py`.  
For further command line arguments use flag `--help`.

If evaluation is split into multiple parts (e.g. first 10k, second 10k),
two result folders will exist and need to be combined for further analysis.  
To do so, use the script `combine.py` as follows:
```
python combine.py <folder1> <folder2> ...
```
The content of files with the same name will be concatenated in the order that the folders are given.  
Compressed files (but only gzip with extension `.gz`) are supported.


### 4) Analyse Collected Suggestions

Start the script `arsr-evaluation/evaluation/analyse.py` using:
```
python analyse.py output/<result_folder> -g resim -mp 1
```

Replace `<result_folder>` with the name of the folder created by `collect.py`.  
By default, this folder contains a timestamp in its name.  

Add `-mp <num>` to set the number of parallel processes to use.  
To use as many as available CPU cores, simply add `-mp 0`.  
The default value of `1` will start the script in single-threaded mode.  
To compress analysis result files, add `-c` to the command.

Created files will be written to `output/analysis_results`.  
This behaviour can be changed using `-o` flag.
Use `--help` for more.


### ARSR Simple

The previous steps produce results for the approaches: `ARSR using Resim, CVR and MFFV`.  
In our final evaluation, we further investigate `ARSR Simple`, which uses string similarity instead of semantic similarity.  
Instructions to reproduce this step as well are provided in the following.

In general, the same steps apply but some commands change:
- In step 2) use `configs/evaluation/api-ncbi-simple-config.json`.
- In step 3) add `--arsr-only` to avoid computing cvr and mffv again.
- In step 4) set `-g simple` (creates sub-folder arsr-simple) and add `-s "_simple"`.

In step 4), a folder `analysis_results_simple` is created.  
It contains the `arsr-simple` folder, which needs to be moved
to the `analysis_results` folder of the previous evaluation for ARSR Resim.

The content of the final `analysis_results` folder should look like below.  
(Note that if compression was used, each file ends with `.csv.gz`.)
```
analysis_results/arsr-resim/arsr*.csv
analysis_results/arsr-simple/arsr*.csv
analysis_results/cvr.csv
analysis_results/mffv.csv
```

Continue with step 5) of the evaluation as usual.



### 5) Compute metrics and plot results

This step computes metrics and creates plots.  
Note that `ARSR Simple` is not required but `analysis_results/arsr-resim` must exist.  
The folder `arsr-resim` stores all results of the `ARSR Resim` approach.

Use the script `plot.py` as follows:
```
python plot.py output/analysis_results/
```

Find the created files in `output/plot_results/`.  
