# Data

This folder contain some of the evaluation data and probably wont include all because of its size.


Currently, this is the included data:
- `cedar/ncbi-ont-rules.zip`
  - CVRE association rules including ontology terms.
  - We extract suitable ontologies from it (not the RDF but their names).
  - Once extracted, we can upload them to an RDF store and make the data accessible for semantic relatedness computation.
  - Furthermore, we can use these rules for evaluation.
- `cedar/test-instances.zip`
  - A collection of instances from the CVRE annotated testing instances.
  - These instances have been formatted by us to contain only relevant information.
  - We will use them to simulate the populated forms during evaluation.
- `cedar/mappings_merged.json`
  - URI mappings provided and used by CVRE.
  - Mappings state which URIs denote the same concepts.

*CVRE = Cedar Value Recommender Evaluation*  
