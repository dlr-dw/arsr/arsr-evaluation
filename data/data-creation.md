# Data Creation
This section briefly shows how evaluation data was created.  
Scripts required for this task are available in the [scripts](../scripts) folder.  
We use [data from the Cedar Value Recommender Evaluation](https://drive.google.com/file/d/1AjcgMi3VM1sYdshAkcUiYhP4QBhqhtfk/view?usp=sharing).  
Please note that (for this data) **their license** ([../data/cedar/LICENSE.txt](../data/cedar/LICENSE.txt)) applies.  
The scripts they used and instructions for creating the data are available at [https://github.com/metadatacenter/cedar-experiments-valuerecommender2019](https://github.com/metadatacenter/cedar-experiments-valuerecommender2019).

Running scripts in the following requires that the **virtual environment** is set up and requirements are installed properly.  
Using a virtual environment is not required but highly suggested.


## A) Preparing Rules Data
Extract the CVR rules data in `data/cedar/ncbi-ont-rules.zip`.  
This directory should now be available: `data/cedar/ncbi-ont-rules/`.  

### A.1) Find useful Ontologies
Next, we search useful ontologies for semantic relatedness computation.

Set the working directory to `scripts/data-extraction` (in this repository).
Retrieve top Ontologies, based on amount of URLs in `fieldValueType`:
```
python extract_ontologies.py -vt -l -s ../../data/cedar/ncbi-ont-rules/ncbi-ont-rules-data.json -o "ontologies_all.json"
```

Get top ontologies per "form field" of a rule item from `fieldValueType`:
```
python extract_ontologies.py -vt -s ../../data/cedar/ncbi-ont-rules/ncbi-ont-rules-data.json -o "ontologies_per-field.json" --field-counts
```

Select some of the overall most popular and per-field popular Ontologies (e.g. 4-10) from the created files.  
Put their URIs in a new file `scripts/data-extraction/input/ontologies_combis.json` as follows:  
```json
{
  "description": "This file contains ontology URIs to create combinations",
  "ontologies": [
    "http://www.ebi.ac.uk/efo",
    "http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl",
    "http://purl.obolibrary.org/obo/PATO",
    "http://purl.obolibrary.org/obo/UBERON",
    "http://purl.bioontology.org/ontology/SNOMEDCT",
    "http://purl.obolibrary.org/obo/CLO",
    "http://phenomebrowser.net/ontologies/mesh/mesh.owl",
    "http://purl.obolibrary.org/obo/BTO",
    "http://purl.obolibrary.org/obo/CL"
   ]
}
```


### A.2) Check Rules Coverage
We check how many rules are covered using combinations of the selected ontologies.  

Working directory is still `scripts/data-extraction`.  
Find top combinations of *s* ontologies (number *s* is set with `-s`):
```
python extract_rules.py combis ../../data/cedar/ncbi-ont-rules/ncbi-ont-rules-data.json -onto-file input/ontologies_combis.json -o top_combinations.jsonl -s 4 --premise-only
```

This script will count how many rules are extracted for a specific combination of *s* ontologies.  
Note that `-onto-file` is the file previously created in step A.1).  
With the flag `--premise-only`, only the lhs (antecedent) of rules is used.  
This is done because only value pairs from the lhs are used during semantic relatedness computation.  
Adding `--no-mappings` allows considering only the `fieldValueType` of rule items.

Find our results for this command in `scripts/data-extraction/output/rules.zip` inside `ncbi/combis`.

Next, have a look at the created file `top_combinations.jsonl` and select a desired combination (e.g. the top most).  
Put these ontologies in a new file `scripts/data-extraction/input/ontologies.json` as follows:
```json
{
  "description": "This file contains ontology URIs for which rules will be extracted.",
  "ontologies": [
    "http://www.ebi.ac.uk/efo",
    "http://purl.obolibrary.org/obo/CL",
    "http://purl.obolibrary.org/obo/BTO",
    "http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl"
  ]
}
```

Note that we put the final selection of 6 ontologies for the evaluation in the file `input/ontologies_final.json`.  
The combination including `PATO` was used in favor of that with `SNOMEDCT`, because `PATO` is smaller and very important for the field `sex`.


### Optional: Check Miss
We can check how many rules we would miss without one of the selected ontologies.  
For that, use the script `scripts/data-extraction/extract_without.py`:
```
python extract_without.py --premise-only ../../data/cedar/ncbi-ont-rules/ncbi-ont-rules-data.json -onto-file input/ontologies.json --out "extracted_ontologies_without.json"
```
This creates a file `extracted_ontologies_without.json` with some more useful information.  
The field "result" lists the input ontologies and how many rules are missed if we would exclude this ontology from our selection.


### A.3) Extract Rules for Ontologies
Extract rules for ontologies of the previously created file (`-onto-file`):
```
python extract_rules.py extract ../../data/cedar/ncbi-ont-rules/ncbi-ont-rules-data.json -onto-file input/ontologies.json --premise-only -o extracted_rules -jl --export-rdf
```
This command checks that rule items contain a URI from these ontologies within their antecedent.  
Checking the rule consequent as well can be done by removing `--premise-only`.  
However, including the consequent would not have any benefit for the evaluation, as only the premise is used for semantic relatedness computation.

The script further filters out the field `treatment`, as it was not part of the CVR evaluation.  
Because treatment can sometimes be the only item of an association rules side (i.e. lhs or rhs),
it can happen that some rules do not have a single item on one side.  
We skip such rules during extraction, as at least one item on each side is required.  
The printout shows the amount of skipped rules like this: "Rules skipped because one side was empty: 220".

A JSON Lines (.jsonl) file and an RDF Turtle (.ttl) file were created:
- `extracted_rules.jsonl`
	- Contains one rule per line.
	- Rules from this file are used as input rules for recommendation.
- `extracted_rules.ttl`
	- Contains triples that link nodes of different ontologies that represent the same entity (`owl:sameAs`).
	- This file is loaded to the RDF store after loading the selected Ontologies.

Find our extracted rules in `output/rules.zip` inside `ncbi/extracted/`.  
For the final rules, `input/ontologies_final.json` was used in the last command as the input for `-onto-file`.


## B) Preparing Form Instances
Extract the CVR rules data in `data/cedar/ncbi-ont-rules.zip`.  
This directory should now be available: `data/cedar/ncbi-ont-rules/`.  

### B.1) Get Testing Instances Data
Extract `cedar_instances_annotated.zip` from the [data of CEDAR Value Recommender Evaluation](https://drive.google.com/file/d/1AjcgMi3VM1sYdshAkcUiYhP4QBhqhtfk/view?usp=sharing).  

Extract the "root" folder `ncbi_cedar_instances/testing` from it, which should contain:
- instances_110001to120000/
- instances_120001to130000/
- instances_130001to140000/

### B.2) Combine JSON Files
We combine the JSON files of each folder (e.g. `instances_110001to120000/`) to a single file.  
This makes it easier to compress and read.

For each of the 3 folders inside the root folder:
1. Open the script `scripts/instance-formatting/combine_instances.py`
2. Uncomment these two lines and adjust `INSTANCES_PATH` to point at the folder:
	- INSTANCES_PATH = 'my_path/instances_110001to120000'
	- OUTPUT_FILENAME = 'instances_110001to120000.jsonl'
3. Run the script: `python combine_instances.py`

Each script execution creates a file in the folder the script was run from (if not changed).
Each created file contains all form instances previously present in the respective folder.  

Put all the created `.jsonl` files in a new folder named `instances_jsonl`.


### B.3) Format Instances
Each instance contains more information than required by us.  
We only extract the required information and format it.  

Open the script `scripts/instance-formatting/format_instances.py`:
- Set the `INSTANCES_PATH` at the path to the previously created folder (`instances_jsonl`).
- Adjust `OUTPUT_FILENAME` if required.

Now run the script: `python format_instances.py`

A new file is created (e.g. `ncbi_instances_formatted.jsonl`).  
It contains all formatted instances from the files of the input folder.  
This file is used by the main evaluation script to provide form instances.
