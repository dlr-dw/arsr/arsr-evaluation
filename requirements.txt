# For sending requests to REST API
# https://docs.python-requests.org/en/latest/
# Source: https://github.com/psf/requests
# (Apache-2.0 License)
requests>=2.27.1

# Used by the plotting scripts to process the data.
# https://pandas.pydata.org/
# Source: https://github.com/pandas-dev/pandas
# (BSD-3-Clause License)
# Note: scripts created with pandas version 1.4.x
pandas>=1.3.5

# Used for actual plotting.
# https://github.com/matplotlib/matplotlib
# Note: scripts created and tested with version 3.5.2
matplotlib

# Also used for plotting (e.g. heatmap).
# https://github.com/mwaskom/seaborn
# Note: scripts created with version 0.11.2
seaborn
