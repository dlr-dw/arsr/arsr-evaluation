# Combine multiple instances JSON files to a single JSON Lines file.
# The file size can be reduced a lot (around 12x) when compressing
# the JSON Lines file compared to all instances in own JSON files.
# (Part of Master's Thesis Leon H. 2022)
# (Created: 24.03.2022)

import json
import os
import pathlib
import sys


# Select the path and output accordingly before running the script:

#INSTANCES_PATH = './instances_110001to120000'
#OUTPUT_FILENAME = 'instances_110001to120000.jsonl'

#INSTANCES_PATH = './instances_120001to130000'
#OUTPUT_FILENAME = 'instances_120001to130000.jsonl'

#INSTANCES_PATH = './instances_130001to140000'
#OUTPUT_FILENAME = 'instances_130001to140000.jsonl'


def main():
	path = pathlib.Path(INSTANCES_PATH)
	if path.exists() and not path.is_dir():
		print('Invalid instances path (needs to point at a folder).')
		return 1

	abspath = path.resolve()
	print(f'Checking instances in: {abspath}')
	print(f'Writing to: {OUTPUT_FILENAME}')
	with open(OUTPUT_FILENAME, 'w') as fout:
		for dirpath, dirnames, filenames in os.walk(abspath):
			for filename in filenames:
				filepath = os.path.join(dirpath, filename)
				with open(filepath, 'r') as fin:
					fout.write(fin.read())
					fout.write('\n')

	print('Done.')
	return 0


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
