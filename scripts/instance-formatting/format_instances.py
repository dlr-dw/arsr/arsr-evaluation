# Format form instances stored in JSONL (or JSON) files.
# The exported instances will be in JSON Lines format and
# only contain information required for the evaluation.
# (Part of Master's Thesis Leon H. 2022)
# (Created: 01.04.2022)

import json
import os.path
import pathlib
import sys
import urllib.parse


# Set input and output accordingly before running the script!
INSTANCES_PATH = './instances_jsonl' # must be a folder
OUTPUT_FILENAME = 'ncbi_instances_formatted.jsonl'

# only extract the listed fields
FIELDS_NCBI = {'sex', 'tissue', 'cell_line', 'cell_type', 'disease', 'ethnicity'}
FIELDS_EBI = {'sex', 'organismPart', 'cellLine', 'cellType', 'diseaseState', 'ethnicity'}
FIELDS = set.union(FIELDS_NCBI, FIELDS_EBI)


def main():
	path = pathlib.Path(INSTANCES_PATH)
	if path.exists() and not path.is_dir():
		print('Invalid instances path (needs to point at a folder).')
		return 1

	abspath = path.resolve()
	print(f'Using instances in: {abspath}')
	print(f'Writing to: {OUTPUT_FILENAME}')
	with open(OUTPUT_FILENAME, 'w') as fout:
		for dirpath, dirnames, filenames in os.walk(abspath):
			for filename in filenames:
				filepath = os.path.join(dirpath, filename)
				with open(filepath, 'r') as fin:
					process_file(fin, fout)

	print('Done.')
	return 0


def process_file(file_in, file_out):
	""" Process an instance file. """

	def format_and_export(instance, fout, filename):
		""" Format and write out the instance. """
		out = format_instance(instance, filename)
		fout.write(json.dumps(out))
		fout.write('\n')

	filename = os.path.basename(file_in.name)
	print(f'Processing file: {filename}')

	if filename.endswith('.json'):
		try:
			instance = json.load(file_in)
			format_and_export(instance, file_out, filename)
		except Exception as ex:
			print(f'Failed to convert instance: {filename} - {ex}')

	elif filename.endswith('.jsonl'):
		line_num = 0
		for line in file_in:
			line_num += 1
			try:
				instance = json.loads(line)
				format_and_export(instance, file_out, filename)
			except Exception as ex:
				print(f'Failed to convert instance: {filename} (Line {line_num}) - {ex}')
	else:
		print(f'Invalid file format, skipping: {filename}')


def format_instance(instance, source_filename):
	""" Get uniform format of instances. """

	# format and add relevant fields
	formatted_fields = {}
	for field in FIELDS:
		field_data = instance.get(field, None)
		if field_data is None: continue
		formatted_fields[field] = format_field(field_data)

	return {
		'id': instance['@id'],
		'source': source_filename,
		'fields': formatted_fields
	}


def format_field(value):
	""" Get a uniform format of field data. """

	value_label = None
	if '@value' in value:
		value_label = value['@value']
	elif 'rdfs:label' in value:
		value_label = value['rdfs:label']

	value_type = None
	if '@id' in value:
		value_type = value['@id']
	
	field_type = None
	if '@type' in value:
		field_type = value['@type']

		# decode (e.g. http://data.bioontology.org/ontologies/EFO/classes/http%3A%2F%2Fwww.ebi.ac.uk%2Fefo%2FEFO_0001799)
		sep = field_type.split('/classes/')
		if len(sep) > 1:
			quoted_url = sep[1]
			field_type = urllib.parse.unquote(quoted_url)

	def add_if_set(out, value, key):
		""" Add value with key to out dict if value is not None. """
		if value is None: return
		out[key] = value

	out = {}
	add_if_set(out, value_label, 'value_label')
	add_if_set(out, value_type, 'value_type')
	add_if_set(out, field_type, 'field_type')
	return out


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
