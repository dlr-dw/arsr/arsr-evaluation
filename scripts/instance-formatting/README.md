# Instance Formatting Scripts

These scripts were used to convert the form instances provided by the Cedar Value Recommender evaluation.  
The final result of the extracted and formatted data is available in `data/cedar/` inside `test-instances.zip`.


## Reproduction

The following describes how to reproduce the steps that lead to the formatted test instances.  
As input data, we use [data from the Cedar Value Recommender Evaluation](https://drive.google.com/file/d/1AjcgMi3VM1sYdshAkcUiYhP4QBhqhtfk/view?usp=sharing).  
*Note: The previous link leads to their Google Drive upload (file size about 1.5GB).*  
Here is a link to their [github repository](https://github.com/metadatacenter/cedar-experiments-valuerecommender2019), which includes the evaluation Jupyter Notebook.  


### Step 1

From the downloaded `data.zip`, extract: `data\cedar_instances\cedar_instances_annotated.zip` (500MB).  
Open this archive (`cedar_instances_annotated.zip`) as well and extract either:
- `cedar_instances_annotated\ebi_cedar_instances\testing\`
- `cedar_instances_annotated\ncbi_cedar_instances\testing\`

The folder `testing` contains three folders named `instances_...to...`.  
Copy the script `combine_instances.py` into the `testing` folder.  


### Step 2

Edit `combine_instances.py` to uncomment the according `INSTANCES_PATH` and `OUTPUT_FILENAME`
for each folder separately and run the script with:
```
python combine_instances.py
```

After doing this for each of the three folders, there will be three files:
- instances_110001to120000.jsonl
- instances_120001to130000.jsonl
- instances_130001to140000.jsonl


### Step 3

Create a folder called `instances_jsonl` inside the folder where the `format_instances.py` script is located.  
Then move the three files previously created (in [step 2](#step-2)) to this new folder.  


### Step 4

Edit `format_instances.py` and check that `INSTANCES_PATH` points to the new folder (`instances_jsonl/`) correctly.  
Also adjust `OUTPUT_FILENAME` if required.  

Now run the script to format the instances:
```
python format_instances.py
```

This script creates the final result file for either ebi or ncbi
(depending on what was used in [step 1](#step-1)).  
