import argparse
import json
import os
import pathlib
import sys


FIELDS_KEY = 'fields'
FIELD_VALUE_KEY = 'value_type'
FIELD_VALUE_FALLBACK_KEY = 'value_label'


def main():
	parser = prepare_parser()
	args = parser.parse_args()

	instances_path = pathlib.Path(args.input)
	if not instances_path.is_file() or not str(instances_path).lower().endswith('.jsonl'):
		print(f'Invalid instances file: {instances_path}')
		return 1

	output_dir = pathlib.Path(args.output)
	if not output_dir.exists():
		print(f'Creating directory: {output_dir}')
		os.makedirs(output_dir, exist_ok=True)
	if not output_dir.is_dir():
		print(f'Invalid output directory: {output_dir}')
		return 1

	print(f'Processing file: {instances_path}')
	field_values = dict()
	lno = 0
	with instances_path.open('r') as file_in:

		for jstr in file_in:
			lno += 1
			jdata = json.loads(jstr)
			fields = jdata.get(FIELDS_KEY)
			if fields is None or not isinstance(fields, dict):
				print(f'Missing "fields" (line {lno}).')
				continue

			for fname, fdata in fields.items():
				fvalue = fdata.get(FIELD_VALUE_KEY)
				if fvalue is None: fvalue = fdata.get(FIELD_VALUE_FALLBACK_KEY)
				if fvalue is None: continue
				if fname not in field_values: field_values[fname] = set()
				if isinstance(fvalue, list): field_values[fname].update(fvalue)
				else: field_values[fname].add(fvalue)

	# print field value counts
	counts = {field: len(values) for field, values in field_values.items()}
	print(json.dumps(counts, indent=4))

	# export results
	out_file = os.path.join(output_dir, 'unique-field-values.json')
	print(f'Writing: {out_file}')
	with open(out_file, 'w') as file_out:
		result = {field: list(values) for field, values in field_values.items()}
		json.dump(result, file_out, indent=2)

	out_file = os.path.join(output_dir, 'instances-stats.json')
	print(f'Writing: {out_file}')
	with open(out_file, 'w') as file_out:
		result = {
			'input-file': os.path.basename(instances_path),
			'line-count': lno,
			'unique-values-per-field': counts
		}
		json.dump(result, file_out, indent=2)

	return 0


def prepare_parser():
	""" Prepare argument parsing. """
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('input', help='Path to formatted instances file (.jsonl)', type=str)
	parser.add_argument('-o', '--output', help='Path to write results to', type=str, default='./output')
	return parser


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
