# Data Stats Scripts

## instance_stats.py
Get stats about the formatted form instances that are used as test instances in the evaluation.  

Usage example:
```
python instance_stats.py ../../data/cedar/test-instances/ncbi_instances_formatted.jsonl
```

Make sure that the folder `test-instances` exists.  
If not, extract the corresponding zip-archive first.  
