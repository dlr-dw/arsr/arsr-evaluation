Difference between `ontologies.json` and `ontologies_final.json`:  
- `ontologies.json` is a selection of the "top 4" ontologies to cover many rules.
- `ontologies_final.json` additionally includes `PATO` and `UBERON` ontologies, as they are important for some form fields.
  - We used this file to extract the rules for the evaluation.
