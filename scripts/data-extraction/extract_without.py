#!/usr/bin/env python3

# With this script, we can check how many rules we would "lose"
# if we exclude any of the provided ontologies.
# (Part of Master's Thesis Leon H. 2022)
#
## Suggested run commands:
# python .\extract_without.py ..\..\data\ncbi-ont-rules\ncbi-ont-rules-data.json -onto-file .\input\ontologies.json --premise-only
#
## Following flags can be added for additional information (for more run with --help):
# -vt or --value-type-only : Only use URIs from fieldValueType
# -o or --out : Specify output file name
# -mmf or --max-miss-fraction : If fraction of items missed is more than this value, a rule is lost

import argparse
import itertools
import json
import pathlib
import sys
from collections import OrderedDict

import extract_utils


# input and output file encoding
FILE_ENCODING = 'utf8'


def main():
	parser = prepare_parser()
	if len(sys.argv) == 1:
		parser.print_help()
		return 1

	# parse arguments and check that input file is valid
	args = parser.parse_args()
	path = pathlib.Path(args.file)
	if not (path.exists() and path.is_file()):
		print('Invalid file.')
		return 1

	# load ontology URIs
	ontologies = set()
	if args.ontologies is not None:
		ontologies.update(args.ontologies)
	if args.ontologies_file is not None:
		ontologies.update(ontologies_from_file(args.ontologies_file))

	if len(ontologies) == 0:
		print('No ontologies given.')
		return 1

	# prepare file for writing output (create it or notify if exists)
	out_path, created = extract_utils.prepare_file_write(args.out)
	if out_path is None:
		print('Invalid output file.')
		return 1
	if out_path.exists() and not created and not args.force:
		print(f'Output file already exists. Use `--force` to overwrite.')
		return 1

	# skip fields that are not required
	fields_to_skip = ['treatment']
	premise_only = args.premise_only
	value_type_only = args.value_type_only
	mmf = args.max_miss_fraction

	with path.open('r', encoding=FILE_ENCODING) as fin:
		print(f'Processing: {fin.name}')
		rules_lost, line_count, line_fails = process(
			fin,
			ontologies=ontologies,
			fields_to_skip=fields_to_skip,
			premise_only=premise_only,
			max_miss_fraction=mmf,
			value_type_only=value_type_only
		)

	print(f'Writing results to: {out_path.resolve()}')
	with out_path.open('w', encoding=FILE_ENCODING) as fout:
		# sort by frequency
		result = sorted(rules_lost.items(), key=lambda e: e[1], reverse=True)
		out = {
			'description': 'Result shows how many rules are lost if the respective ontology is excluded.',
			'rules_total': line_count,
			'rules_fails': line_fails,
			'fields_skipped': fields_to_skip,
			'rule_parts_used': 'lhs' if premise_only else 'lhs & rhs',
			'uri_fields_used': 'fieldValueType' if value_type_only else 'fieldValueType & fieldValueMappings',
			'max_miss_fraction': mmf,
			'result': OrderedDict(result),
		}
		json.dump(out, fout, indent=2)

	print('Done.')
	return 0


def ontologies_from_file(path):
	""" Get ontology URIs from file or command line arguments. """
	onto_path = pathlib.Path(path)
	if onto_path.exists() and onto_path.is_file():
		print(f'Loading ontologies from file: {onto_path.resolve()}')
		with onto_path.open('r', encoding=FILE_ENCODING) as file: jdata = json.load(file)
		if 'ontologies' in jdata: return jdata['ontologies']
		print('Failed to load URIs from file: Missing field "ontologies".')
	return []


def process(file_handle, ontologies, fields_to_skip=None, premise_only=False, max_miss_fraction=0.0,
			value_type_only=False):
	""" Count for each ontology, how many rules we lose if we don't use it.

	If values of rule items are not covered by the other ontologies, a rule is treated as "lost".
	Not covered means that there is no ontology to which any term-URI of a (rule item) value belongs to.

	Parameters
	----------
	ontologies : typing.Collection[str]
		List of ontology URIs.
	fields_to_skip : typing.List[str]
		Items with a field name in this list will be ignored.
	premise_only : bool
		Check only items of the rule premise and ignore consequent.
	max_miss_fraction : float
		A rule counts as "lost" if a fraction of more than this value of valid items is not covered.
		Default of 0 means that a rule is lost, if any of its items is not covered (i.e. 100% must be covered).
		E.g. a value of 1/6 means that 1 out of 6 items can be missed.
	value_type_only : bool
		Use only the field `fieldValueType` as source for URIs of a rule item value.
	"""
	rules_lost = {o: 0 for o in ontologies}
	lno, fail = 0, 0

	for line in file_handle:
		lno += 1
		try:
			jrule = json.loads(line)
		except Exception as ex:
			print(f'Failed to process line {lno}: {ex}')
			fail += 1
			continue

		if lno % 1000 == 0:  # print progress
			print(f'Rules processed: {lno}')

		# get rule data
		rsource = jrule['_source']
		lhs = rsource['premise']
		rhs = rsource['consequence']

		for onto_url in ontologies:
			if premise_only: items = lhs
			else: items = itertools.chain(lhs, rhs)

			# how many items do we lose if we discard this ontology
			items_missed = 0
			items_total = 0
			for item in items:
				field, label = item_info(item)
				if field.lower() in fields_to_skip: continue
				items_total += 1
				uris = suitable_uris(item, ontologies, onto_url, value_type_only)
				if len(uris) == 0:
					items_missed += 1

			# could happen if all items are skipped
			if items_total == 0: continue

			# check if this rule counts as "lost"
			missed_perc = items_missed / items_total
			if missed_perc > max_miss_fraction:
				rules_lost[onto_url] += 1

	return rules_lost, lno, fail


def item_info(_item: dict):
	""" Get field and label info from the item dict. """
	field = str(_item['fieldPath'])
	label = str(_item['fieldValueLabel'])
	return field, label


def suitable_uris(_item: dict, onto_list, onto_to_skip: str, value_type_only: bool):
	""" Returns a list of term-URIs that are part of the ontologies. """
	uris = []
	for onto in onto_list:
		if onto == onto_to_skip: continue
		if _item['fieldValueType'].startswith(onto):
			uris.append(_item['fieldValueType'])
		if not value_type_only:
			for uri in _item['fieldValueMappings']:
				if uri.startswith(onto): uris.append(uri)
	return uris


def prepare_parser():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
		description='Extract rules from CEDAR Value Recommender evaluation data from 2019.'
	)
	parser.add_argument('file', help='Path to rules file')
	parser.add_argument('-onto', '--ontologies', help='Ontology URIs', nargs='+', metavar='URI')
	parser.add_argument('-onto-file', '--ontologies-file', help='Path to JSON file that contains ontology URIs')
	parser.add_argument('-o', '--out', help='Name of output file', default='extracted_ontologies_without.json')
	parser.add_argument('-f', '--force', help='Overwrite output file if it exists', action='store_true')
	parser.add_argument('-mmf', '--max-miss-fraction', help='Fraction of items that can be missed', type=float, default=0)
	parser.add_argument('-po', '--premise-only', help='Only check items of rule premise', action='store_true')
	parser.add_argument('-vt', '--value-type-only', help='Use only URIs from `fieldValueType`', action='store_true')
	return parser


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
