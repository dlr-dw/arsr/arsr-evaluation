# Data Extraction (Rules Extraction)

This folder contains scripts to extract relevant rules from the Cedar Value Recommender's evaluation data.  

Problem definition:
- An RDF store is required that contains information to compute semantic relatedness between nodes.
- Rules use URIs from various ontologies and not just one.
- A subset of these ontologies needs to be found that covers many rules.


----
Script `extract_ontologies.py` allows to extract ontologies (or rather their main URIs) present in the rules data.  
Run it with the flag `--help` to see possible options.  
Results of this script are available in `output/ontologies.zip`.  


----
Script `extract_rules.py` allows to compute what combinations of ontologies from a subset cover how many rules,
and further allows to extract rules for a given set of ontologies (their main URIs).  
Run it with the flag `--help` to see possible options.  
Results of this script are available in `output/rules.zip`.  
This script produces the input rules used for the main evaluation.  

Note that the order of the selected ontologies (e.g. in `input/ontologies.json`) matters in so far
that the ones "earlier" in the list are preferred over the others.
Thererfore, the "earlier" entries more likely define the `value` of a rule item if alternative URIs from URI mappings are used.  
This way, more "specific" ontologies can be placed earlier to prefer their usage in the export.  

For the evaluation, we extracted rules where each item has a URI in `fieldValueType` that belongs to one of the 6 ontologies.  
An additional set of rules (R2) with alternative URIs from the mappings was also used.

The final rules (for NCBI) are available under: `output/rules/ncbi/extracted/final/`.  
Here is the command used to generate them (run from location of python script):
```
python extract_rules.py extract --premise-only --no-uris --no-alternatives -onto-file ./input/ontologies_final.json ../../data/cedar/ncbi-ont-rules/ncbi-ont-rules-data.json -o extracted-rules_ncbi -jl
```

Find the additional triples linking the ontologies in `output/rules/triples/ontologies-final_lhs-only/`.  
They were created with alternative URIs, which makes no big difference but adds some more useful data to the RDF store.  


----
Script `extract_without.py` allows to check how many rules one would "lose" if any of the provided ontologies is excluded.  
It allows to further verify which of the ontologies from the selected subset are important to cover many rules.  

However, the most important scripts are the first two mentioned in this README.  
