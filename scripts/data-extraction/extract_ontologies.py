#!/usr/bin/env python3

# This script allows finding common ontologies in
# the data of the CEDAR Value Recommender Evaluation.
# (Part of Master's Thesis Leon H. 2022)
#
#
## What it does
# It uses items of the lhs (or optional also rhs) of rules and the URIs
# provided in the fields "fieldValueType" and "fieldValueMappings".
# Based on these URIs, we count how often each ontology occurs.
#
#
## Suggested run commands:
# python extract_ontologies.py -n 10 -vt -f <rules.json> -o <output.json>
# => Extract top 10 ontologies from "fieldValueTypes"
#
# python extract_ontologies.py -n 10 -vm -f <rules.json> -o <output.json>
# => Extract top 10 ontologies from "fieldValueMappings"
#
#
## Following flags can be added for additional information:
# -l : extract original links as well and store in a field "links"
# -s : additionally split purl.obolibrary.org URLs
# -f : force file overwrite if export file already exists
# --rules-common : find ontologies that often occur together in rules
# --field-counts : count how often ontologies occur per field
# --add-consequent : use items of the rule consequent (rhs) as well

import argparse
from collections import OrderedDict
import itertools
import json
import pathlib
import sys

import extract_utils


def main():

	parser = prepare_parser()
	if len(sys.argv) == 1:
		parser.print_help()
		return 1

	# parse arguments and check that input file is valid
	args = parser.parse_args()
	path = pathlib.Path(args.file)
	if not (path.exists() and path.is_file()):
		print('Invalid file.')
		return 1

	# process the rules file (one rule in JSON format per line)
	with path.open('r', encoding='utf8') as file:
		print(f'Processing {path.resolve()}')
		result = process_file(
			file_handle=file,
			export_links=args.export_links,
			value_type=args.value_type,
			value_mappings=args.value_mappings,
			split_obo=args.split_obo,
			add_consequent=args.add_consequent,
			rules_common=args.rules_common,
			combi_r=2,
			common_freq_min=args.rules_common_min,  # set 0 for all
			field_counts=args.field_counts,
			field_counts_top_n=args.field_counts_top  # set -1 to print all
		)

	ontologies = result['ontologies']
	rule_counts = result['rule_counts']

	# sort top n by rules count
	top_n = get_top(args.n, ontologies, sort_by_key='rules')

	if args.out is None or len(args.out) == 0 or args.out == 'print':
		print(f'\nTop {args.n} ontologies:')
		for pos, onto_uri, onto_data in top_n:
			print(f'[{pos}]: {onto_uri}')
	else:
		out_path, created = extract_utils.prepare_file_write(args.out)
		if out_path is None:
			print('Invalid output file.')
			return 1
		if out_path.exists() and not created and not args.force:
			print(f'Output file already exists. Use `--force` to overwrite.')
			return 1
		else:
			export_to_file(out_path, result, top_n, args)

	print(f'\nRules total: {rule_counts[0]}, failed: {rule_counts[1]}')
	print(f'Ontologies total: {len(ontologies)}')


def process_file(file_handle, export_links, value_type=True,
	value_mappings=False, split_obo=False, add_consequent=False,
	rules_common=False, combi_r=2, common_freq_min=0,
	field_counts=False, field_counts_top_n=-1):
	""" Each line is a json object and will be processed.

	Parameters
	----------
	export_links : bool
		Add the original URIs to the output.
	value_type : bool
		Use URIs from field `fieldValueType`. 
	value_mappings : bool
		Use URIs from field `fieldValueMappings`.
	split_obo : bool
		Treat "purl.obolibrary.org/obo/" URLs differently by splitting
		them at "_" to extract the actual Ontology.
	add_consequent : bool
		Include items from the consequent (rhs) of the rule.
	rules_common : bool
		Find ontologies that occur together and count how often.
	combi_r : int
		If `rules_common` is True, this is the size of each combination tuple
		of ontologies that occur together. Default is 2.
	common_freq_min : int
		How often ontologies must at least occur together to be added
		to the result of `rules_common`. Default is 2.
	field_counts : bool
		Count how often each ontology occurs per field.
	field_counts_top_n : int
		Only add the top n ontologies when counting occurrences in fields.
		Default: 10 (set -1 to disable).
	"""

	ontologies = dict()
	common_ontos_rules = dict() # for rules_common
	field_ontos = dict() # for field_counts
	field_freqs = dict() # for field_counts field frequencies

	lno = 0
	fail = 0
	for line in file_handle:
		lno += 1
		try:
			jrule = json.loads(line)
		except Exception as ex:
			print(f'Failed to process line {lno}: {ex}')
			fail += 1
			continue

		if lno % 1000 == 0: # print progress
			print(f'Rules processed: {lno}')

		# store all ontologies of this rule
		rule_ontos = set() # for rules_common

		# store which ontologies rule count was already increased
		rulecount_added = set()

		# count ontology occurrences
		for item in get_rule_items(jrule, add_consequent):

			uris = set()
			if value_type: uris.add(item['fieldValueType'])
			if value_mappings: uris.update(item['fieldValueMappings'])

			field = item['fieldPath']

			for uri in uris:
				onto = extract_utils.get_ontology(uri, split_obo)

				if onto not in ontologies:
					ontologies[onto] = {
						'count': 0,
						'rules': 0
					}
					if export_links:  # unmodified URLs
						ontologies[onto]['links'] = set()

				# increase general occurrency counter
				ontologies[onto]['count'] += 1

				# increase rule counter
				if onto not in rulecount_added:
					ontologies[onto]['rules'] += 1
					rulecount_added.add(onto)

				# add uri to its links and store rule onto
				if export_links: ontologies[onto]['links'].add(uri)
				if rules_common: rule_ontos.add(onto)

				# gather field onto counts
				if field_counts:
					# onto count
					if field not in field_ontos: field_ontos[field] = dict()
					if onto not in field_ontos[field]: field_ontos[field][onto] = 1
					else: field_ontos[field][onto] += 1

			# gather field total counts
			if field_counts:
				if field not in field_freqs: field_freqs[field] = 1
				else: field_freqs[field] += 1

		# create every combination and store or increase count
		if rules_common:
			combis = itertools.combinations(sorted(rule_ontos), combi_r)
			for c in combis:
				if c not in common_ontos_rules: common_ontos_rules[c] = 1
				else: common_ontos_rules[c] += 1

	# turn sets into lists so that json can deserialize it
	if export_links:
		for onto in ontologies:
			ontologies[onto]['links'] = list(ontologies[onto]['links'])

	# prepare result
	result = {
		'ontologies': ontologies,
		'rule_counts': (lno, fail)
	}

	# convert common ontologies information and add it
	if rules_common:
		common_list = []
		for onto_tuple, count in common_ontos_rules.items():
			if count < common_freq_min: continue # skip entry
			common_list.append({
				'ontologies': onto_tuple,
				'frequency': count
			})
		sort_func = lambda e: e['frequency']
		result['common_in_rules'] = sorted(common_list, key=sort_func, reverse=True)

	# sort ontologies per field accordingly and add top n
	if field_counts:
		fc_result = dict()
		for field, fontos in field_ontos.items():
			fontos_sorted = sorted(fontos.items(), key=lambda e: e[1], reverse=True)
			if field_counts_top_n >= 0: # get top n
				fontos_sorted = fontos_sorted[:field_counts_top_n]
			fc_result[field] = OrderedDict(fontos_sorted)
		result['field_counts'] = fc_result
		result['field_counts_total'] = field_freqs

	return result


def get_rule_items(jrule, add_consequent=False):
	""" Returns a generator that yields each rules lhs item. """
	def item_info(_item):
		return {
			'fieldPath': _item['fieldPath'],
			'fieldType': _item['fieldType'],
			'fieldValueLabel': _item['fieldValueLabel'],
			'fieldValueType': _item['fieldValueType'],
			'fieldValueMappings': _item['fieldValueMappings']
		}

	for item in jrule['_source']['premise']:
		yield item_info(item)

	if add_consequent:
		for item in jrule['_source']['consequence']:
			yield item_info(item)


def get_top(n, ontologies, sort_by_key='rules'):
	""" Extracts top 10 ontologies and returns (pos, uri, data) tuple. """
	cnt = 0
	get_all = n < 1
	sort_func = lambda e: e[1][sort_by_key]  # sort by count
	for onto in sorted(ontologies.items(), key=sort_func, reverse=True):
		if cnt < n or get_all: yield cnt+1, onto[0], onto[1]
		cnt += 1


def export_to_file(out_path, result, top_n, args):
	""" Writes results to the file. """
	result_ontologies = []
	for pos, onto_uri, onto_data in top_n:
		result_ontologies.append({
			'onto': onto_uri,
			'stats': onto_data
		})

	def fields_used():
		if args.value_type: yield 'fieldValueType'
		if args.value_mappings: yield 'fieldValueMappings'

	def items_from():
		yield 'lhs'
		if args.add_consequent: yield 'rhs'

	jdata = OrderedDict([
		('ruleCount', result['rule_counts'][0]),
		('ruleFails', result['rule_counts'][1]),
		('ontoCount', len(result['ontologies'])),
		('usedFields', list(fields_used())),
		('oboSplit', args.split_obo),
		('itemsFrom', list(items_from())),
		('ontologies', result_ontologies)
	])

	if 'common_in_rules' in result:
		jdata['common_in_rules'] = result['common_in_rules']
		jdata['common_freq_min'] = args.rules_common_min

	if 'field_counts' in result:
		jdata['field_counts_top'] = args.field_counts_top
		jdata['field_counts_total'] = result['field_counts_total']
		jdata['field_counts'] = result['field_counts']

	print(f'Writing to file: {out_path.resolve()}')
	with out_path.open('w', encoding='utf8') as fout:
		json.dump(jdata, fout, indent=2)


def prepare_parser():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
		description='Find ontologies in rules data of CEDAR Value Recommender evaluation data from 2019.'
	)
	parser.add_argument('file', help='Path to rules file')
	parser.add_argument('-n', '--n', help='Find top n ontologies (set -1 for all)', type=int, default=10)
	parser.add_argument('-o', '--out', help='Name of output file (set `print` for printout only)', default='extracted_ontologies.json', required=False)
	parser.add_argument('-f', '--force', help='Overwrite output file if it exists', action='store_true')
	parser.add_argument('-vt', '--value-type', help='Extract URIs from field `fieldValueType`', action='store_true')
	parser.add_argument('-vm', '--value-mappings', help='Extract URIs from field `fieldValueMappings`', action='store_true')
	parser.add_argument('-l', '--export-links', help='Export the original links that belong to a URI', action='store_true')
	parser.add_argument('-s', '--split-obo', help='Split URLs starting with purl.obolibrary based on underscore', action='store_true')
	parser.add_argument('--rules-common', help='Find ontologies that often occur together in rules', action='store_true')
	parser.add_argument('--rules-common-min', help='Min common count to be added to output (set 0 for all)', type=int, default=10)
	parser.add_argument('--field-counts', help='Count how often ontologies occur per field', action='store_true')
	parser.add_argument('--field-counts-top', help='Only add top n field counts (set -1 to add all)', type=int, default=10)
	parser.add_argument('--add-consequent', help='Add items from rhs of rule as well', action='store_true')
	return parser


if __name__ == '__main__':
	main()
