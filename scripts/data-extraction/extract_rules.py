#!/usr/bin/env python3

# This script allows to extract rules for specific ontologies
# from the data of the CEDAR Value Recommender Evaluation.
# (Part of Master's Thesis Leon H. 2022)
#
#
## Result
# Exports rules that match the criteria (min. perc. of items with a URI that
# starts with that of any of the given ontologies). The field "uris" contains
# the matching URIs and the field "uri" the basic field type.
# The "uri" is set anyway, even if it is not matching any ontology so that
# there is least one URI use if min_perc_items is below 1.
# Note that the order of the "ontologies" list affects what URI is used in the field "value" of a rule.
# This way, one can place the more specific ontologies higher in the list (i.e. closer to index 0) to prefer it.
#
#
## Suggested run commands:
# python extract_rules.py extract -onto-file .\input\ontologies.json ..\..\data\cedar\ncbi-ont-rules\ncbi-ont-rules-data.json --out rules -jl
# => Extract rules and store in JSON Lines format
#
# python extract_rules.py combis -onto-file .\input\ontologies_combis.json ..\..\data\cedar\ncbi-ont-rules\ncbi-ont-rules-data.json
# => Try out all combinations of the provided ontologies and get amount of rules each covers
#
#
## Following flags can be added for additional information:
# -mpi : fraction of items that must contain a URI of any given ontology for the rule to be added (range 0 to 1)
# -f : force file overwrite if export file already exists
# -jl : write results to json lines format
# -t : (for extract command only) number of transactions used for rule mining to make support relative (rounded to 8 digits after comma)
#   NOTE that output of json.dumps produces scientific notation for very small values!
#   You can set -t 0 to disable the conversion and keep absolute values instead.
# --premise-only : consider only items of rule premise (lhs) to decide if a rule should be added
# --export-rdf : (for extract command only) Export RDF triples that connect selected ontologies
# --no-mappings : disable use of fieldValueMappings to find suitable URIs and only use fieldValueType
# --no-alternatives : disable using alternative URIs from URI mappings if URI from type is not valid


import argparse
from collections import OrderedDict
import json
import sys
import pathlib
import os.path
import itertools
import time
import unicodedata  # for string normalization

import extract_utils


# see https://www.w3.org/TR/2004/REC-owl-guide-20040210/#Namespaces
RDF_OWL_NAMESPACE = 'http://www.w3.org/2002/07/owl#'

# skip adding relations between terms from same ontology
RDF_SKIP_SAME_ONTOS = True

# If an items type URI is not one of the selected ontologies, use a valid URI from the items URI mappings instead.
# If there is no other, the original is kept. Disable from command line using "--no-alternatives".
REPLACE_TYPE_URI = True

# Input and export file encoding
FILE_ENCODING = 'utf8'


# Following variables can be changed from command line:

# If False, only consider fieldValueType and not fieldValueMappings
USE_MAPPINGS = True  # disable using "--no-mappings"

# If False, the "uris" field of rules won't be exported.
EXPORT_URIS = True  # disable using "--no-uris"


def main():

	parser = prepare_parser()
	if len(sys.argv) == 1:
		parser.print_help()
		return 1

	# parse arguments and check that input file is valid
	args = parser.parse_args()
	path = pathlib.Path(args.file)
	if not (path.exists() and path.is_file()):
		print('Invalid file.')
		return 1

	global USE_MAPPINGS
	USE_MAPPINGS = False if args.no_mappings else True
	if not USE_MAPPINGS:
		print('Ignoring fieldValueMappings.')

	global EXPORT_URIS
	if 'no_uris' in args: EXPORT_URIS = args.no_uris
	if EXPORT_URIS is False:
		print('Exporting uri mappings per rule disabled.')

	global REPLACE_TYPE_URI
	if args.no_alternatives: REPLACE_TYPE_URI = False
	print(f'Use alternative URIs: {REPLACE_TYPE_URI}')

	# load ontology URIs, deduplicate and ensure to keep order
	ontologies = OrderedDict()
	if args.ontologies is not None:
		ontologies.update(((o, None) for o in args.ontologies))
	if args.ontologies_file is not None:
		onto_list = ontologies_from_file(args.ontologies_file)
		ontologies.update(((o, None) for o in onto_list))
	ontologies = [o for o in ontologies]  # convert back to list

	allow_all = args.allow_all if 'allow_all' in args else False
	if len(ontologies) == 0:
		if allow_all is False:
			print('No ontologies given.')
			return 1
		else:
			# 1 element required to work
			ontologies = ['none']


	command = args.command

	# get output file path
	output_file = args.out
	if command == 'extract':
		if args.json_lines:
			if output_file.endswith('.json'):
				output_file = output_file[:-5]
			if not output_file.endswith('.jsonl'):
				output_file += '.jsonl'
		elif not output_file.endswith('.json'):
			output_file += '.json'

	# prepare file for writing output (create it or notify if exists)
	out_path, created = extract_utils.prepare_file_write(output_file)
	if out_path is None:
		print('Invalid output file.')
		return 1
	if out_path.exists() and not created and not args.force:
		print(f'Output file already exists. Use `--force` to overwrite.')
		return 1


	if command == 'combis':

		onto_combis = list(itertools.combinations(ontologies, args.combi_size))
		combi_count = {}
		print(f'Combinations: {len(onto_combis)}')
		time.sleep(1)

		print(f'Processing...')
		for i in range(len(onto_combis)):
			combi = onto_combis[i]

			with path.open('r', encoding=FILE_ENCODING) as file:
				result = extract_rules(
					file_handle=file,
					ontologies=list(combi),
					min_perc_items=args.min_perc_items,
					fields_to_skip=['treatment'],
					premise_only=args.premise_only
				)

			combi_count[i] = len(result['rules'])
			print(f'Progress: {round(i / len(onto_combis) * 100, 2):.2f}%')

		print('Writing results to file...')
		with out_path.open('w', encoding=FILE_ENCODING) as fout:
			res = sorted(combi_count.items(), key=lambda e: e[1], reverse=True)
			for idx, rule_count in res:
				fout.write(json.dumps({
					'rules': rule_count,
					'combi': onto_combis[idx]
				}))
				fout.write('\n')

	elif command == 'extract':

		# process the rules file (one rule in JSON format per line)
		with path.open('r', encoding=FILE_ENCODING) as file:
			print(f'Processing {path.resolve()}')
			result = extract_rules(
				file_handle=file,
				ontologies=ontologies,
				num_transactions=args.num_transactions,
				min_perc_items=args.min_perc_items,
				fields_to_skip=['treatment'],
				premise_only=args.premise_only,
				export_rdf=args.export_rdf,
				allow_all=args.allow_all
			)

		rules, counts = result['rules'], result['line_counts']
		print(f'\nRules extracted: {len(rules)}/{counts[0]} (fails: {counts[1]})')
		export_to_file(out_path, ontologies, result, args)


def ontologies_from_file(path):
	""" Get ontology URIs from file or command line arguments. """
	onto_path = pathlib.Path(path)
	if onto_path.exists() and onto_path.is_file():
		print(f'Loading ontologies from file: {onto_path.resolve()}')
		with onto_path.open('r') as file: jdata = json.load(file)
		if 'ontologies' in jdata: return jdata['ontologies']
		print('Failed to load URIs from file: Missing field "ontologies".')
	return []


class Rule:
	""" Temp. rule class for export. """

	def __init__(self, support, confidence):
		self.lhs = []
		self.rhs = []
		self.sup = support
		self.con = confidence

	def to_dict(self, export_uris=True):
		return {
			'lhs': [item.to_dict(export_uris) for item in self.lhs],
			'rhs': [item.to_dict(export_uris) for item in self.rhs],
			'sup': self.sup,
			'con': self.con
		}


class RuleItem:
	""" Temp. class for rule items. """

	def __init__(self, field, label, uri=None, uris=None):
		self.field = field
		self.label = label
		self.uri = uri  # base URI
		self.uris = [] if uris is None else uris

		# prefer using URI as value if set
		has_uri = self.uri is not None and len(self.uri) > 0
		self.value = self.uri if has_uri else self.label
		if not has_uri:
			print(f'Item without URI: {self.label} (field: {self.field})')

	def to_dict(self, export_uris=True):
		""" Convert item to dict for export.
		This is the format that works with the Suggestion Service.
		"""
		rule_dict = {
			'field': self.field,
			'value': self.value,
			'label': self.label
		}
		if export_uris: rule_dict['uris'] = self.uris
		return rule_dict


def extract_rules(
	file_handle, ontologies, num_transactions=0, min_perc_items=1, fields_to_skip=None,
	premise_only=False, export_rdf=True, allow_all=False):
	""" Extract rules for given ontologies.

	Parameters
	----------
	file_handle : typing.IO
	ontologies : list[str]
	num_transactions : int
		Number of transactions used for rule mining.
		Reason: CEDAR Value Recommender used absolute values for rule support.
		Therefore, we need to convert it to relative support.
		Default is 0 and disables the conversion.
	min_perc_items : float
		Min percentage of items (in range 0 to 1) that must have a URI for a rule to be exported.
		If set to 1, all items must have a URI for their field value.
	fields_to_skip : list[str]
		Items with a field name in this list will be discarded.
	premise_only : bool
		Check only items of the rule premise and ignore consequent.
	export_rdf : bool
		Extract all URIs used for the same item value. This means that these
		URIs denote the same thing and should later be linked through owl:sameAs.
	allow_all : bool
		Allow all ontologies. Effectively, this just formats the input rules.
	"""
	rdfsame = dict()  # one set per field value that contains URIs
	rules_out = []
	lno, fail = 0, 0
	skipped_because_empty = 0

	for line in file_handle:
		lno += 1
		try:
			jrule = json.loads(line)
		except Exception as ex:
			print(f'Failed to process line {lno}: {ex}')
			fail += 1
			continue

		if lno % 1000 == 0:  # print progress
			print(f'Rules processed: {lno}')

		# get rule data
		rsource = jrule['_source']
		lhs = rsource['premise']
		rhs = rsource['consequence']
		support = rsource['support']
		confidence = rsource['confidence']

		# convert support
		if num_transactions > 1:
			support = round(support / num_transactions, 8)

		rule = Rule(support, confidence)

		def select_uris(_item, onto_list, no_check=False, use_mappings=True):
			""" Returns a list of suitable URIs. """
			uris = []
			for onto in onto_list:  # URIs ealier in list are added first => ensures order
				if no_check or _item['fieldValueType'].startswith(onto):
					uris.append(_item['fieldValueType'])
				if use_mappings:
					for uri in _item['fieldValueMappings']:
						if no_check or uri.startswith(onto): uris.append(uri)
			return uris

		def prepare_item(_item, no_check=False):
			""" Creates a RuleItem based on the given item dict. """
			field = _item['fieldPath']
			label = _item['fieldValueLabel']
			uri = _item['fieldValueType']
			uris = select_uris(_item, ontologies, no_check, use_mappings=USE_MAPPINGS)
			uris_len = len(uris)
			if REPLACE_TYPE_URI and uris_len > 0:
				# if the type uri is no term from any ontology, replace it
				# but still keep the replacement uri in the list for RDF mappings
				if uri not in uris: uri = uris[0]  # select a "valid" URI
			return RuleItem(field, label, uri=uri, uris=uris), uris_len

		items_without_uri_lhs = 0
		items_without_uri_rhs = 0
		skip_fields = len(fields_to_skip) > 0

		for item in lhs:
			ritem, uris_count = prepare_item(item, no_check=allow_all)
			if skip_fields and ritem.field in fields_to_skip: continue
			uri_invalid = ritem.uri not in ritem.uris  # can happen if alternatives are disabled
			if uris_count == 0 or uri_invalid: items_without_uri_lhs += 1
			rule.lhs.append(ritem)

		for item in rhs:
			ritem, uris_count = prepare_item(item, no_check=allow_all)
			if skip_fields and ritem.field in fields_to_skip: continue
			uri_invalid = ritem.uri not in ritem.uris  # can happen if alternatives are disabled
			if uris_count == 0 or uri_invalid: items_without_uri_rhs += 1
			rule.rhs.append(ritem)

		# sum up items without valid uri
		items_without_uri = items_without_uri_lhs
		if not premise_only: items_without_uri += items_without_uri_rhs

		# check that rule has at least one item in lhs and one in rhs
		side_empty = []
		if len(rule.lhs) == 0: side_empty.append('lhs')
		if len(rule.rhs) == 0: side_empty.append('rhs')
		if len(side_empty) > 0:
			# print(f'Skipping rule (line {lno}): No {" & ".join(side_empty)} items after processing.')
			skipped_because_empty += 1
			continue

		# check if this rule should be added
		items_total = len(lhs) + len(rhs)
		items_perc = 1 - items_without_uri / items_total

		if not (items_perc < min_perc_items):
			rules_out.append(rule.to_dict(EXPORT_URIS))

			if export_rdf:  # store information for rdf export
				for ritem in itertools.chain(rule.lhs, rule.rhs):
					if len(ritem.uris) == 0: continue
					val = unicodedata.normalize('NFKC', ritem.label).lower()
					if val not in rdfsame: rdfsame[val] = set()
					rdfsame[val].update(ritem.uris)

	if skipped_because_empty > 0:
		print(f'Rules skipped because one side was empty: {skipped_because_empty}')

	result = {
		'rules': rules_out,
		'line_counts': (lno, fail)
	}

	if export_rdf:
		result['export_rdf'] = rdfsame

	return result


def export_to_file(out_path, ontologies, result, args):
	""" Writes results to the file. """

	rules = result['rules']
	line_counts = result['line_counts']

	print(f'Writing to file: {out_path.resolve()}')
	if args.json_lines:
		# write out as one JSON entry per line
		with out_path.open('w', encoding=FILE_ENCODING) as fout:
			for rule in rules:
				jrule = json.dumps(rule)
				fout.write(jrule + '\n')
	else:
		# write as one big JSON file
		jdata = OrderedDict([
			('ontologies', list(ontologies)),
			('rules_total', line_counts[0]),
			('premise_only', args.premise_only),
			('rules_extracted', len(rules)),
			('rules', rules)
		])

		with out_path.open('w', encoding=FILE_ENCODING) as fout:
			json.dump(jdata, fout, indent=2)

	# Additional export of RDF Turtle file:
	# write triples to file (it's the easiest format without using parsers)
	if 'export_rdf' in result:

		rdfpath = pathlib.Path(os.path.splitext(str(out_path))[0] + '.ttl')
		print(f'Writing RDF data to file: {str(rdfpath)}')

		with rdfpath.open('w', encoding=FILE_ENCODING) as fout:

			# write owl prefix to file
			fout.write(f'@prefix owl: <{RDF_OWL_NAMESPACE}> .\n')

			# write simple triples to file
			added = set()
			for val, uris in result['export_rdf'].items():
				if len(uris) == 0: break
				combis = itertools.combinations(uris, 2)
				for uri1, uri2 in combis:
					if (uri1, uri2) in added: continue
					# check if uris are likely from same ontology
					ont1 = extract_utils.get_ontology(uri1, True)
					ont2 = extract_utils.get_ontology(uri2, True)
					if ont1.lower() == ont2.lower(): continue
					# add triple to output
					added.add((uri1, uri2))
					fout.write(f'<{uri1}> owl:sameAs <{uri2}> .\n')


def prepare_parser():
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
		description='Extract rules from CEDAR Value Recommender evaluation data from 2019.'
	)

	def add_defaultargs(p):
		p.add_argument('file', help='Path to rules file')
		p.add_argument('-onto', '--ontologies', help='Ontology URIs', nargs='+', metavar='URI')
		p.add_argument('-onto-file', '--ontologies-file', help='Path to JSON file that contains ontology URIs')
		p.add_argument('-mpi', '--min-perc-items', help='Fraction of items that must contain a URI of any given ontology for the rule to be added (range 0 to 1)', type=float, default=1)
		p.add_argument('--premise-only', help='Only check that items of rule premise match', action='store_true')
		p.add_argument('--no-mappings', help='Disable use of URIs in fieldValueMappings', action='store_true')
		p.add_argument('-no-alt', '--no-alternatives', help='Disable replacing item URI with one from URIs', action='store_true')
		p.add_argument('-f', '--force', help='Overwrite output file if it exists', action='store_true')

	subparsers = parser.add_subparsers(dest='command')
	parser_extract = subparsers.add_parser(
		name='extract',
		help='Extract rules for given ontologies',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)
	add_defaultargs(parser_extract)
	parser_extract.add_argument('-o', '--out', help='Name of output file', default='extracted_rules.json')
	parser_extract.add_argument('-jl', '--json-lines', help='Export rules in JSON Lines format without additional information', action='store_true')
	parser_extract.add_argument('-t', '--num-transactions', help='Total number of transactions used for rule mining', type=int, default=114909)
	parser_extract.add_argument('-all', '--allow-all', help='Allow all ontologies (just format rules)', action='store_true')
	parser_extract.add_argument('--no-uris', help='Do not export the "uris" field of rules', action='store_false')
	parser_extract.add_argument('--export-rdf', help='Export rdf triples that connect selected ontologies', action='store_true')

	parser_combis = subparsers.add_parser(
		name='combis',
		help='Find combinations of ontologies that cover most rules',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)
	add_defaultargs(parser_combis)
	parser_combis.add_argument('-o', '--out', help='Name of output file (JSON Lines format only)', default='top_combis.jsonl')
	parser_combis.add_argument('-s', '--combi-size', help='Amount of ontologies per combination', type=int, default=3)
	return parser


if __name__ == '__main__':
	main()
