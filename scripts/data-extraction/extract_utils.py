# Helper methods used by extraction scripts

import pathlib


def get_ontology(uri, split_obo=False):
	""" Try to extract the ontology URI without the entity. """
	# if there is a fragment, it is probably the entity
	result = uri.rsplit('#', 1)
	if len(result) > 1: return result[0]

	# split obo-URLs differently
	if split_obo and uri.startswith('http://purl.obolibrary.org/obo/'):
		return uri.split('_', 1)[0]

	# if there is no fragment, the last part of the path is probably the entity
	result = uri.rsplit('/', 1)
	return result[0]


def prepare_file_write(filepath):
	""" Creates file and the directories if missing.
	Returns the pathlib.Path or None if file is invalid and bool if created.
	"""
	path = pathlib.Path(filepath)
	created = False
	if not path.exists():  # try creating
		path.parent.mkdir(parents=True, exist_ok=True)  # create dirs
		try:
			path.open('x').close()  # create file
			created = True
		except FileExistsError:
			pass
	if not path.is_file(): path = None
	return path, created
