#!/usr/bin/env python3

# Compare recommendation results of our approach to results
# that use the approach of the Cedar Value Recommender (CVR).
# This script collects the results for evaluation test combinations.
# After running it, use analyse.py to extract useful information.
# If evaluation was split in two parts, use combine.py to create a combined result folder.
# (Part of Master's Thesis Leon H. 2022)
# 
# Goals of the evaluation:
# - Our approach replaces the "context_matching_score" used by CVR.
# - We want to compare how this change affects the overall recommendations.
# - One possibility to judge the quality is to use the MRR (mean reciprocal rank).
# - As we only sort based on the final score, the MRR computation needs to be adjusted
#   so that recommendations with the same score are treated like they've got the same rank.
#
# How it's done:
# - For our approach to work, we need an RDF store with suitable data.
# - This data are ontologies that contain the used terms for field values.
# - We therefore extract required ontologies and rules from the evaluation data of the CVR.
# - These rules are used as an input to our system in addition to the annotated test instances
#   that were also used by the CVR evaluation.
# - With rules and test forms available, we can run both scoring methods.
#
# What this evaluation doesn't cover:
# - CVR does not take fields with multiple values into account like we do.
# - E.g. it can not predict more values for a populated field, based on its current values.
# - Furthermore, rules are considered to have just a single item on the rhs. of a rule.
# - For these reasons, this evaluation is restricted on data that both systems can handle.
#
# Main code of the CVR evaluation:
# https://github.com/metadatacenter/cedar-experiments-valuerecommender2019/blob/master/scripts/arm_evaluation_main.py

import argparse
import logging
import signal
import sys
import time

from evaluation import logs, constants
from evaluation.measurement import Measurement
from evaluation.evaluator import Evaluator
from evaluation.utils import create_dirs


if (sys.version_info[0] < 3) or (sys.version_info[1] < 7):
	raise Exception('This script requires Python 3.7+.')

LOGGER: logging.Logger = logging.getLogger()


def main():

	# get command line arguments
	parser = prepare_parser()
	args = parser.parse_args()
	instances_path = args.instances
	instances_max = args.instances_max
	start_from = args.start_instance

	# get start timestamp for file association
	timestamp = Measurement.get_timestamp()

	# suffix for result folder and log file
	suffix = args.result_suffix
	if len(suffix.strip()) == 0: suffix = 'timestamp'
	if suffix.lower() == 'timestamp': suffix = timestamp

	# prepare output folder (i.a. for logs and results)
	out_path = create_dirs(constants.OUTPUT_DIRECTORY, print)
	log_path = out_path.joinpath(f'logs_{suffix}.log')
	global LOGGER
	LOGGER = logs.configure_logging(
		debug=args.debug,
		file_debug=args.debug_file,
		filepath=log_path
	)

	# prepare result folder
	folder_name = f'result_{suffix}'
	out_path = out_path.joinpath(folder_name)
	if out_path.exists():
		LOGGER.warning(f'Abort. Output folder already exists: {out_path}')
		return 1
	out_path = create_dirs(out_path, LOGGER.info)

	# prepare evaluation class
	measurement = Measurement(output_dir=out_path, arsr_only=args.arsr_only)
	evaluator = Evaluator(measurement)

	# load most frequent field values if path is given
	mffv_path = constants.FREQUENT_VALUES_PATH
	if mffv_path is not None:
		measurement.load_frequent_field_values(mffv_path)

	LOGGER.info(f'Starting evaluation with instance {start_from}. Make sure that API is running.')
	time.sleep(3)

	def sigint_handler(signum, frame):
		LOGGER.warning(f'Signal received ({signum}). Initiating graceful shutdown...')
		evaluator.abort()

	# register shutdown signal handler for graceful shutdown
	signal.signal(signal.SIGINT, sigint_handler)

	t_start = time.perf_counter()
	evaluator.start_evaluation(instances_path, start_from, instances_max)
	t_total = round(time.perf_counter() - t_start, 2)

	# close open file streams
	measurement.cleanup()

	if len(measurement.get_failures()) > 0:
		LOGGER.warning(f'Recommendation failures: {measurement.get_failures()}')

	LOGGER.info(f'Instances processed: {evaluator.instances_processed}, skipped: {evaluator.instances_skipped}')
	LOGGER.info(f'Runtime: {t_total} seconds')
	LOGGER.info('Exit.')
	return 0


def prepare_parser():
	""" Prepare argument parsing. """
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('-i', '--instances', help='Path to instances file', type=str, default=constants.INSTANCES_PATH)
	parser.add_argument('-imax', '--instances-max', help='Max number of instances (-1 = unlimited)', type=int, default=constants.INSTANCES_MAX)
	parser.add_argument('-s', '--start-instance', help='Start from instance n and skip previous', type=int, default=1)
	parser.add_argument('-ao', '--arsr-only', help='Run ARSR only and no baselines', action='store_true')
	parser.add_argument('-rs', '--result-suffix', help='Result folder suffix (result_<suffix>)', type=str, default='timestamp')
	parser.add_argument('-db', '--debug', help='Set logging level to debug', action='store_true')
	parser.add_argument('-dbf', '--debug-file', help='Set file logging level to debug', action='store_true')
	return parser


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
