#!/usr/bin/env python3

# Analyse collected recommendations results and
# extract useful information (e.g. pos of expected value).
# Results of this script can be used for plotting.
# (Part of Master's Thesis Leon H. 2022)

import argparse
import logging
import os
import pathlib
import sys

from evaluation import logs, constants
from evaluation.utils import create_dirs
from evaluation.analysis import Analysis


if (sys.version_info[0] < 3) or (sys.version_info[1] < 7):
	raise Exception('This script requires Python 3.7+.')

LOGGER: logging.Logger = logging.getLogger()


def main():

	# get command line arguments
	parser = prepare_parser()
	args = parser.parse_args()

	# prepare output folder (i.a. for logs and results)
	out_path = create_dirs(constants.OUTPUT_DIRECTORY, print)
	log_path = out_path.joinpath(f'analysis_log.log')
	global LOGGER
	LOGGER = logs.configure_logging(
		debug=args.debug,
		file_debug=args.debug_file,
		filepath=log_path
	)

	# abort if output directory already exists
	output_dir = args.output
	if args.output_suffix is not None:
		output_dir += args.output_suffix
	output_path = pathlib.Path(output_dir)
	if output_path.exists():
		LOGGER.error(f'Output directory already exists: {output_path.resolve()}')
		return 1

	# set score precision to be rounded at x digits after comma
	score_rounding_precision = 6
	discard_duplicate_entries = False if args.allow_duplicates else True
	analysis = Analysis(
		score_rounding_precision, discard_duplicate_entries,
		args.max_processes, args.group_arsr
	)

	if constants.USE_URI_MAPPINGS:
		success = analysis.load_uri_mappings(constants.URI_MAPPINGS_PATH)
		if not success: return 1

	try:
		analysis.analyse_dir(args.input, str(output_path), args.compress)
	except Exception:
		LOGGER.exception('Analysis failed.')
		return 1
	return 0


def prepare_parser():
	""" Prepare argument parsing. """
	default_out_dir = os.path.join(constants.OUTPUT_DIRECTORY, 'analysis_results')
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('input', help='Path to recommendation result(s)', type=str)
	parser.add_argument('-g', '--group-arsr', help='Folder suffix to group ARSR files (e.g. resim)', type=str)
	parser.add_argument('-o', '--output', help='Path to write analysis results to', type=str, default=default_out_dir)
	parser.add_argument('-s', '--output-suffix', help='Add this prefix to output folder name', type=str)
	parser.add_argument('-c', '--compress', help='Compress output files (using gzip)', action='store_true')
	parser.add_argument('-mp', '--max-processes', help='Processes for parallel execution (0=#cores)', type=int, default=1)
	parser.add_argument('-ad', '--allow-duplicates', help='Do not skip export of duplicate entries', action='store_true')
	parser.add_argument('-db', '--debug', help='Set logging level to debug', action='store_true')
	parser.add_argument('-dbf', '--debug-file', help='Set file logging level to debug', action='store_true')
	return parser


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
