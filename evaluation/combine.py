import argparse
import gzip
import logging
import os.path
import pathlib
import sys

from evaluation import constants, logs
from evaluation.utils import create_dirs


LOGGER: logging.Logger = logging.getLogger()


def main():

	# get command line arguments
	parser = prepare_parser()
	args = parser.parse_args()

	global LOGGER
	LOGGER = logs.configure_logging()

	# prepare output directory
	output_dir = args.output
	if args.output_suffix is not None:
		output_dir += args.output_suffix
	output_path = pathlib.Path(output_dir)
	if output_path.exists():
		LOGGER.error(f'Output directory already exists: {output_path.resolve()}')
		return 1
	output_path = create_dirs(output_dir, LOGGER.info)

	# find files and gather paths for files of same name
	folders = args.folders
	LOGGER.info(f'Folders to combine (in this order): {folders}')
	file_paths = gather_files(args.folders)
	LOGGER.info(f'Files found: {list(file_paths.keys())}')

	# combine content of files with same name
	for filename, paths in file_paths.items():
		out_filepath = output_path.joinpath(filename)
		extension = os.path.splitext(filename)[1]
		if extension is None or len(extension) == 0:
			LOGGER.error(f'Skipping file (missing extension): {filename}')
			continue
		compressed = extension == '.gz'
		combine_content(paths, out_filepath, compressed)

	LOGGER.info('Done.')
	return 0


def gather_files(folders: list):
	""" Gather files from folders and collect their paths. """
	files = dict()
	for folder in folders:
		for dirpath, dirnames, filenames in os.walk(folder):
			for filename in filenames:
				filepath = os.path.join(dirpath, filename)
				if filename not in files: files[filename] = []
				files[filename].append(filepath)
			break
	return files


def combine_content(filepaths: list, out_filepath: str, compressed: bool):
	""" Combine content of these files and write to output file. """
	LOGGER.info(f'Creating combined file (compression: {compressed}): {out_filepath}')
	open_func = gzip.open if compressed else open
	with open_func(out_filepath, 'w') as file_out:
		for path in filepaths:
			LOGGER.info(f'- Adding: {path}')
			with open_func(path, 'r') as file_in:
				file_out.write(file_in.read())


def prepare_parser():
	""" Prepare argument parsing. """
	default_out_dir = os.path.join(constants.OUTPUT_DIRECTORY, 'results_combined')
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('folders', help='Paths to result folders', type=str, nargs='*')
	parser.add_argument('-o', '--output', help='Path to write combined result to', type=str, default=default_out_dir)
	parser.add_argument('-s', '--output-suffix', help='Add this prefix to output folder name', type=str)
	return parser


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
