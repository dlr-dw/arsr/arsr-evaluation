import argparse
import logging
import os
import pathlib
import sys

from evaluation import logs, constants
from evaluation.plotting import Plotting
from evaluation.utils import create_dirs


if (sys.version_info[0] < 3) or (sys.version_info[1] < 7):
	raise Exception('This script requires Python 3.7+.')

LOGGER: logging.Logger = logging.getLogger()


def main():

	# get command line arguments
	parser = prepare_parser()
	args = parser.parse_args()

	# prepare output folder (i.a. for logs and results)
	out_path = create_dirs(constants.OUTPUT_DIRECTORY, print)
	log_path = out_path.joinpath(f'plot_logs.log')
	global LOGGER
	LOGGER = logs.configure_logging(debug=False, file_debug=False, filepath=log_path)

	# prepare output directory
	output_dir = args.output
	if args.output_suffix is not None:
		output_dir += args.output_suffix
	output_path = pathlib.Path(output_dir)
	if output_path.exists():
		LOGGER.error(f'Output directory already exists: {output_path.resolve()}')
		return 1
	output_path = create_dirs(output_dir, LOGGER.info)

	try:
		plotting = Plotting(debug_print=args.debug)
		plotting.process_dir(args.input, str(output_path), args.arsr_variant)
	except Exception:
		LOGGER.exception('Plotting failed.')
		return 1
	return 0


def prepare_parser():
	""" Prepare argument parsing. """
	default_out_dir = os.path.join(constants.OUTPUT_DIRECTORY, 'plot_results')
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('input', help='Path to analysis result(s)', type=str)
	parser.add_argument('-o', '--output', help='Path to write plot results to', type=str, default=default_out_dir)
	parser.add_argument('-av', '--arsr-variant', help='Variant of ARSR to use (e.g. mean-max)', type=str, default='best')
	parser.add_argument('-s', '--output-suffix', help='Add this prefix to output folder name', type=str)
	parser.add_argument('-db', '--debug', help='Set logging level to debug', action='store_true')
	return parser


if __name__ == '__main__':
	exit_code = main()
	if exit_code is None:
		exit_code = 0
	sys.exit(exit_code)
