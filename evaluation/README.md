# Evaluation Scripts

This folder contains the main evaluation scripts.  
Setup and instructions are provided in the main [repository README](../README.md).  

Scripts were tested with `Python 3.7.3` and `Python 3.9.6`.  
At least `Python 3.6+` should be used.  
Make sure to install required dependencies before usage.  

What each script is used for:
1. `collect.py`
    * Reads test instances, creates test combinations and collects recommendation results.
    * Uses the suggestion service API to get recommendations.
    * Provides the MFFV baseline, using values from the given file.
2. `combine.py`
    * Allows to combine result folders, e.g. if evaluation was split in two parts.
    * Usage: `python combine.py <folder1> <folder2> ...`
      * Content of folders is combined in the same order.
3. `analyse.py`
   * Reads result folders and extracts information required for evaluation.
   * For instance, position of expected value.
4. `plot.py`
   * Reads output of analyse script to compute metrics and create plots.
