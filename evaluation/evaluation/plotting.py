import logging
import os
import pathlib
import typing

import pandas as pd

from .analysis import AnalysisKeys
from . import plots


class FileInfo:
	def __init__(self, name: str, path: str):
		self.name = name
		self.path = path
		self.compression = 'gzip' if path.endswith('.gz') else None
		self.stats = {}
		self.variant = None


class ApproachMetrics:
	def __init__(self, df: pd.DataFrame):
		self.mrr5 = df['RR@5'].mean()
		self.mrr10 = df['RR@10'].mean()
		self.mh5 = df['H@5'].mean()
		self.mh10 = df['H@10'].mean()
		self.mcrr5 = df['CRR@5'].mean()  # corrected reciprocal rank
		self.mcrr10 = df['CRR@10'].mean()

	def to_dict(self):
		return {
			'mrr5': self.mrr5, 'mrr10': self.mrr10,
			'mh5': self.mh5, 'mh10': self.mh10,
			'mcrr5': self.mcrr5, 'mcrr10': self.mcrr10
		}

	@classmethod
	def metric_label(cls, name: str, default=None):
		""" Get label for the metric name. """
		name = name.lower()
		return cls.metrics().get(name, default)

	@staticmethod
	def metrics():
		""" Get dict of metrics and respective label. """
		return {
			'mrr5': 'MRR@5', 'mrr10': 'MRR@10',
			'mh5': 'MH@5', 'mh10': 'MH@10',
			'mcrr5': 'MCRR@5', 'mcrr10': 'MCRR@10'
		}


class Plotting:
	""" Compute metrics and plot results. """

	def __init__(self, debug_print=False):
		self.logger = logging.getLogger(__name__)
		self.debug_print = debug_print
		self.stats = {}

	def process_dir(self, data_path: str, output_dir: str, arsr_variant: str):
		""" Process the data in the folder. """
		path = pathlib.Path(data_path)
		if not path.is_dir():
			self.logger.error('Invalid directory path.')
			return

		# retrieve information of required files
		cvr_file = self.__get_file(path, 'cvr.csv.gz', must_exist=False)
		if cvr_file is None: cvr_file = self.__get_file(path, 'cvr.csv')

		mffv_file = self.__get_file(path, 'mffv.csv.gz', must_exist=False)
		if mffv_file is None: mffv_file = self.__get_file(path, 'mffv.csv')

		arsr_resim_files = self.__get_folder_files(str(path.joinpath('arsr-resim')), 'arsr', '.csv')
		arsr_simple_files = self.__get_folder_files(str(path.joinpath('arsr-simple')), 'arsr', '.csv')
		if len(arsr_resim_files) == 0:
			raise IOError('Missing ARSR-Resim files (expected in: arsr-resim/arsr*).')
		if len(arsr_simple_files) == 0:
			self.logger.warning('No files provided for ARSR-simple.')

		# prepare output folders
		self.logger.info('Preparing output folders.')
		output_folder_meta = os.path.join(output_dir, 'meta')
		output_folder_summary = os.path.join(output_dir, 'summary')
		os.makedirs(output_folder_meta, exist_ok=True)
		os.makedirs(output_folder_summary, exist_ok=True)

		# compute stats (same for each approach)
		self.__compute_stats(cvr_file.path, output_folder_meta)

		# find best combi of agg methods and so on
		self.logger.info(f'Analysing ARSR-Resim results ({len(arsr_resim_files)})')
		arsr_resim_mrr10s = self.__analyse_arsr_results(arsr_resim_files, output_folder_summary, 'ARSR-Resim')
		self.logger.info(f'Analysing ARSR-Simple results ({len(arsr_simple_files)})')
		arsr_simple_mrr10s = self.__analyse_arsr_results(arsr_simple_files, output_folder_summary, 'ARSR-Simple')

		# use best variant w.r.t. mrr@10 metric
		arsr_resim_selected = arsr_resim_mrr10s[0][1]
		arsr_simple_selected = arsr_simple_mrr10s[0][1] if len(arsr_simple_mrr10s) > 0 else None

		# select specific variant if specified
		if arsr_variant is not None: arsr_variant = arsr_variant.lower()
		if arsr_variant.lower() != 'best':
			arsr_resim_selected = self.__find_variant('ARSR-Resim', arsr_variant, arsr_resim_files, arsr_resim_selected)
			arsr_simple_selected = self.__find_variant('ARSR-Simple', arsr_variant, arsr_simple_files, arsr_simple_selected)
		self.logger.info(f'Using ARSR-Resim variant {arsr_resim_selected.variant}')
		if arsr_simple_selected is not None:
			self.logger.info(f'Using ARSR-Simple variant {arsr_simple_selected.variant}')

		self.__compare_approaches(
			arsr_resim_selected, arsr_simple_selected,
			cvr_file, mffv_file, output_folder_summary, 'comparison'
		)

	def __find_variant(self, approach: str, arsr_variant, file_infos, current: FileInfo):
		""" Find FileInfo of specified ARSR variant. """
		variant_finfo = None
		if len(file_infos) == 0: return current
		for finfo in file_infos:
			if finfo.variant == arsr_variant:
				variant_finfo = finfo
				break
		if variant_finfo is None:
			self.logger.warning(f'{approach} {arsr_variant} not found.')
			return current
		return variant_finfo

	@staticmethod
	def __get_file(folderpath: pathlib.Path, filename: str, must_exist: bool = True):
		""" Get FileInfo for this file in folder. """
		filepath = folderpath.joinpath(filename)
		if not filepath.exists():
			if must_exist: raise IOError(f'File not found: {filename}')
			else: return None
		if not filepath.is_file(): raise IOError(f'No valid file: {filename}')
		return FileInfo(filepath.name, str(filepath))

	@staticmethod
	def __get_folder_files(folderpath: str, fileprefix: str, fileext: str = '.csv'):
		""" Get FileInfo for each file in the folder that starts with the prefix. """
		files = []
		for dirpath, dirnames, filenames in os.walk(folderpath):
			for filename in filenames:
				fnl = filename.lower()
				# allow the file extension and also ".gz" (compressed)
				if not fnl.endswith(fileext) and not fnl.endswith(fileext + '.gz'): continue
				filepath = os.path.join(dirpath, filename)
				if fnl.startswith(fileprefix):
					files.append(FileInfo(filename, filepath))
			break
		return files

	def __compute_stats(self, filepath: str, output_dir: str):
		""" Computes general stats of data. (They'll be used again later in code.) """
		self.logger.info(f'Computing stats using: {filepath}')
		compression = 'gzip' if filepath.endswith('.gz') else None
		df = pd.read_csv(filepath, compression=compression)

		# frequency per target field
		target_field_series = df[AnalysisKeys.TARGET_FIELD]
		target_field_freq = target_field_series.value_counts().rename('tf-frequency')
		self.stats['target_freq'] = target_field_freq
		# target_field_freq = target_field_freq.rename_axis('tf', axis='index')  # rename index column
		# target_field_freq.to_csv(os.path.join(output_dir, 'target-field_frequency.csv'))

		# unique values per target field
		df_target_expected = df[[AnalysisKeys.TARGET_FIELD, AnalysisKeys.EXPECTED_VALUE]]
		df_te_grouped = df_target_expected.groupby(AnalysisKeys.TARGET_FIELD)
		df_tf_unique = df_te_grouped.nunique().rename(columns={AnalysisKeys.EXPECTED_VALUE: 'ev-unique'})
		self.stats['target_expected_freq'] = df_tf_unique
		# values_unique = values_unique.rename_axis('tf', axis='index')  # rename index column
		# values_unique.to_csv(os.path.join(output_dir, 'target-field_expected-values.csv'))

		# combine information about target field and export as one file
		target_field_info = pd.concat([target_field_freq, df_tf_unique], axis=1)
		target_field_info = target_field_info.rename_axis('target-field', axis='index')
		target_field_info.to_csv(os.path.join(output_dir, 'target-field_frequency.csv'))

		# frequency #populated fields
		populated_counts = df[AnalysisKeys.POPULATED_FIELDS_COUNT]
		populated_freq = populated_counts.value_counts().rename('pf-frequency')
		self.stats['populated_freq'] = populated_freq

		# frequency #populated for max. number of populated fields
		populated_counts_max = df.loc[
			df[AnalysisKeys.POPULATED_FIELDS_COUNT] == df[AnalysisKeys.POPULATED_FIELDS_MAX],  # get where same
			AnalysisKeys.POPULATED_FIELDS_COUNT  # get only column with field counts from the filtered data
		]
		populated_max_freq = populated_counts_max.value_counts().rename('pf-max-frequency')
		self.stats['populated_max_freq'] = populated_max_freq

		# create "table" containing populated frequency summary
		populated_df = pd.concat([populated_freq, populated_max_freq], axis=1)
		populated_df = populated_df.rename_axis('num-populated', axis='index').sort_index()  # rename y-axis and sort
		populated_df.to_csv(os.path.join(output_dir, 'num-populated_frequency.csv'))

		if self.debug_print:
			for stat, info in self.stats.items():
				print(f'\nStat: {stat}')
				print(info)

		# ------------------------------------------------------
		# prepare export folder and plot stats
		plot_dir = os.path.join(output_dir, 'plots')
		os.makedirs(plot_dir, exist_ok=True)
		self.logger.info(f'Creating plots in: {plot_dir}')

		# plot frequency of target fields
		target_field_info = target_field_info.reset_index()  # make index a "real" column for access
		fig = plots.barchart_simple(
			target_field_info, x_column='target-field', y_column='tf-frequency',
			x_label='Target Field', y_label='No. Test Combinations', x_ticks_rotation=30
		)
		fig.savefig(os.path.join(plot_dir, f'frequency-target.png'), bbox_inches='tight')
		fig.savefig(os.path.join(plot_dir, f'frequency-target.pdf'), bbox_inches='tight')
		plots.cleanup(fig)

		# plot unique value count per target field
		fig = plots.barchart_simple(
			target_field_info, x_column='target-field', y_column='ev-unique',
			x_label='Target Field', y_label='No. Unique Values', x_ticks_rotation=30
		)
		fig.savefig(os.path.join(plot_dir, f'unique-values-target.png'), bbox_inches='tight')
		fig.savefig(os.path.join(plot_dir, f'unique-values-target.pdf'), bbox_inches='tight')
		plots.cleanup(fig)

		# plot frequency of #populated
		populated_df = populated_df.reset_index()  # make index a "real" column for access
		fig = plots.barchart_simple(
			populated_df, x_column='num-populated', y_column='pf-frequency',
			x_label='No. Populated Fields', y_label='No. Test Combinations',
			figure_title='Distribution of Populated Field Counts'
		)
		fig.savefig(os.path.join(plot_dir, f'frequency-populated.png'), bbox_inches='tight')
		fig.savefig(os.path.join(plot_dir, f'frequency-populated.pdf'), bbox_inches='tight')
		plots.cleanup(fig)

		# plot frequency of #populated w.r.t. max. no. populated
		fig = plots.barchart_simple(
			populated_df, x_column='num-populated', y_column='pf-max-frequency',
			x_label='No. Populated Fields', y_label='No. Test Combinations',
			figure_title='Distribution for Max. No. Populated Fields'
		)
		fig.savefig(os.path.join(plot_dir, f'frequency-populated-max.png'), bbox_inches='tight')
		fig.savefig(os.path.join(plot_dir, f'frequency-populated-max.pdf'), bbox_inches='tight')
		plots.cleanup(fig)

	@staticmethod
	def __compute_rr(pos: int, at: int = None):
		""" Compute reciprocal rank of this value. """
		in_range = pos > 0 and (at is None or pos < at+1)
		return 1 / pos if in_range else 0

	@classmethod
	def __add_reciprocal_rank(cls, df: pd.DataFrame, col_name: str, newcol_name: str, rr_at: int = 5):
		""" Add reciprocal rank (@ rr_at) as new column with specific name. """
		df[newcol_name] = df[col_name].apply(cls.__compute_rr, at=rr_at)

	@classmethod
	def __add_hit_at(cls, df: pd.DataFrame, col_name: str, newcol_name: str, hit_at: int = 5):
		""" Add hit at metric as new column. """
		df[newcol_name] = df[col_name].apply(lambda pos: 0 < pos < hit_at+1)

	@staticmethod
	def __get_arsr_combi(arsr_filename: str):
		""" Example name: arsr_product-median.csv """
		arsr_filename = arsr_filename.split('.', 1)[0]
		combi = arsr_filename.split('_')[1]
		return combi.split('-')

	def __get_metrics(self, file_info: FileInfo) -> typing.Tuple[pd.DataFrame, ApproachMetrics]:
		""" Read file as dataframe and add metrics. Returns the dataframe and approach metrics. """
		if file_info is None: return None, None
		df = pd.read_csv(file_info.path, compression=file_info.compression)
		self.__add_reciprocal_rank(df, AnalysisKeys.EXPECTED_VALUE_POS, 'RR@5', rr_at=5)
		self.__add_reciprocal_rank(df, AnalysisKeys.EXPECTED_VALUE_POS, 'RR@10', rr_at=10)
		self.__add_hit_at(df, AnalysisKeys.EXPECTED_VALUE_POS, 'H@5', hit_at=5)
		self.__add_hit_at(df, AnalysisKeys.EXPECTED_VALUE_POS, 'H@10', hit_at=10)
		self.__add_reciprocal_rank(df, AnalysisKeys.EXPECTED_VALUE_POS_C1, 'CRR@5', rr_at=5)
		self.__add_reciprocal_rank(df, AnalysisKeys.EXPECTED_VALUE_POS_C1, 'CRR@10', rr_at=10)
		return df, ApproachMetrics(df)

	def __analyse_arsr_results(self, arsr_files: typing.Collection[FileInfo], output_dir: str, relatedness: str):
		""" Analyse ARSR results and return list of (mrr10, FileInfo) tuples with [0] being best mrr10. """
		foldername = relatedness.lower()
		output_dir = os.path.join(output_dir, foldername)
		os.makedirs(output_dir, exist_ok=True)

		data = []
		mrr10s = []
		simple_view = True  # no populated (mrr over all data points)

		for file_info in arsr_files:
			df, metrics = self.__get_metrics(file_info)
			file_info.stats.update(metrics.to_dict())
			# fam = field score aggregation methode
			# sam = suggestion score aggregation method
			fam, sam = self.__get_arsr_combi(file_info.name)
			file_info.variant = f'{fam}-{sam}'
			mrr10s.append((metrics.mrr10, file_info))

			if simple_view:
				row = {'file': file_info.name, 'fam': fam, 'sam': sam}
				row.update(metrics.to_dict())
				data.append(row)
			else:
				populated_min = df[AnalysisKeys.POPULATED_FIELDS_COUNT].min()
				populated_max = df[AnalysisKeys.POPULATED_FIELDS_COUNT].max()
				for i in range(populated_min, populated_max):
					key_pc = AnalysisKeys.POPULATED_FIELDS_COUNT
					# df_pop = df[(df[key_pc] == i) & (df[AnalysisKeys.POPULATED_FIELDS_MAX] == i)]
					df_pop = df[df[key_pc] == i]
					metrics = ApproachMetrics(df_pop)
					row = {'file': file_info.name, 'fam': fam, 'sam': sam, 'populated': i}
					row.update(metrics.to_dict())
					data.append(row)

		if len(data) > 0:
			arsr_df = pd.DataFrame(data)
			arsr_df.sort_values('mrr10', axis=0, ascending=False, inplace=True)
			# arsr_df = arsr_df.round(6)  # round all floats at n decimal places
			arsr_df.to_csv(os.path.join(output_dir, 'arsr-variants-summary.csv'), index=False, float_format='%.6f')
			self.logger.info(f'Best {foldername} variant (MRR@10): fam={arsr_df.iloc[0]["fam"]}, sam={arsr_df.iloc[0]["sam"]}')
			if self.debug_print: print(arsr_df)

			# plot results
			plot_dir = os.path.join(output_dir, 'variant-plots')
			os.makedirs(plot_dir, exist_ok=True)
			self.logger.info(f'Creating plots in: {plot_dir}')
			for metric, label in ApproachMetrics.metrics().items():
				fig = plots.heatmap_arsr(
					arsr_df, x='fam', y='sam', metric_column=metric, metric_label=label,
					figure_title=f'{relatedness} Aggregation Methods'
				)
				fig.savefig(os.path.join(plot_dir, f'variants-{metric}.png'), bbox_inches='tight')
				fig.savefig(os.path.join(plot_dir, f'variants-{metric}.pdf'), bbox_inches='tight')
				plots.cleanup(fig)

		# sorted list of (score, FileInfo) tuples with [0] as best score
		return sorted(mrr10s, key=lambda e: e[0], reverse=True)

	@staticmethod
	def __make_approach_row(approach: str, metrics: ApproachMetrics):
		""" Creates a dict for a row describing approach metrics in a pandas DataFrame. """
		row = {'approach': approach}
		row.update(metrics.to_dict())
		return row

	def __compare_approaches(self, arsr_resim, arsr_simple, cvr, mffv, output_dir, foldername=None):
		""" Compares the approaches based on the metrics.

		Parameters
		----------
		arsr_resim : FileInfo
		arsr_simple : FileInfo | None
		cvr : FileInfo
		mffv : FileInfo
		output_dir : str
		foldername : str | None
		"""
		if foldername is not None:
			output_dir = os.path.join(output_dir, foldername)
			os.makedirs(output_dir, exist_ok=True)

		df_arsr_resim, metrics_arsr_resim = self.__get_metrics(arsr_resim)
		df_arsr_simple, metrics_arsr_simple = self.__get_metrics(arsr_simple)
		df_cvr, metrics_cvr = self.__get_metrics(cvr)
		df_mffv, metrics_mffv = self.__get_metrics(mffv)

		arsr_resim_label = f'ARSR-Resim {arsr_resim.variant}'
		arsr_simple_label = f'ARSR-Simple {arsr_simple.variant}' if arsr_simple is not None else None
		cvr_label = 'CVR'
		mffv_label = 'MFFV'

		data = [self.__make_approach_row(arsr_resim_label, metrics_arsr_resim)]
		if arsr_simple is not None:
			data.append(self.__make_approach_row(arsr_simple_label, metrics_arsr_simple))
		data.extend([
			self.__make_approach_row(cvr_label, metrics_cvr),
			self.__make_approach_row(mffv_label, metrics_mffv)
		])
		df_comparison = pd.DataFrame(data)
		df_comparison.to_csv(os.path.join(output_dir, 'approaches.csv'), index=False, float_format='%.6f')
		self.logger.info('Approach comparison done.')
		if self.debug_print: print(df_comparison)

		# plot and export images
		plot_dir = os.path.join(output_dir, 'approaches-plots')
		os.makedirs(plot_dir, exist_ok=True)
		self.logger.info(f'Creating plots in: {plot_dir}')
		for metric, label in ApproachMetrics.metrics().items():
			fig = plots.barchart_approaches(
				df_comparison, metric_column=metric, metric_label=label,
				figure_title=None
			)
			fig.savefig(os.path.join(plot_dir, f'approaches-{metric}.png'), bbox_inches='tight')
			fig.savefig(os.path.join(plot_dir, f'approaches-{metric}.pdf'), bbox_inches='tight')
			plots.cleanup(fig)

		# compare approach metrics w.r.t. #populated
		dframes = [(df_arsr_resim, arsr_resim_label)]
		if df_arsr_simple is not None: dframes.append((df_arsr_simple, arsr_simple_label))
		dframes.extend([(df_cvr, cvr_label), (df_mffv, mffv_label)])
		populated_df: pd.DataFrame = self.stats['populated_freq'].rename_axis('populated', axis='index').reset_index()
		populated_nums = populated_df.sort_values('populated')['populated'].to_list()  # = [1, 2, 3, ...] based on data
		self.__compare_populated(dframes, populated_nums, output_dir, 'approaches_populated.csv', False, 'approaches-populated-plots')
		self.__compare_populated(dframes, populated_nums, output_dir, 'approaches_populated-max.csv', True, 'approaches-populated-max-plots')

		# compare approach metrics w.r.t. target fields
		target_df: pd.DataFrame = self.stats['target_freq'].rename_axis('target', axis='index').reset_index()
		target_fields = target_df['target'].to_list()
		self.__compare_target(dframes, target_fields, output_dir, 'approaches_target.csv', False, 'approaches-target-plots')
		self.__compare_target(dframes, target_fields, output_dir, 'approaches_target_populated-max.csv', True, 'approaches-target-popmax-plots')

	def __compare_populated(self, dframes, populated_nums, output_dir, out_name, pop_max: bool, plotfolder: str):
		""" Compare approaches w.r.t. number of populated fields. """
		data = []
		for populated_count in populated_nums:
			for df, label in dframes:
				if pop_max:
					df_pop_only = df[
						(df[AnalysisKeys.POPULATED_FIELDS_COUNT] == populated_count) &
						(df[AnalysisKeys.POPULATED_FIELDS_MAX] == populated_count)
					]
				else:
					df_pop_only = df[df[AnalysisKeys.POPULATED_FIELDS_COUNT] == populated_count]
				df_metrics = ApproachMetrics(df_pop_only)
				row = {'populated': populated_count}
				row.update(self.__make_approach_row(label, df_metrics))
				data.append(row)
		df_comparison_pop = pd.DataFrame(data)
		df_comparison_pop.to_csv(os.path.join(output_dir, out_name), index=False, float_format='%.6f')
		self.logger.info(f'File created: {out_name}')
		if self.debug_print: print(df_comparison_pop)

		# plot and export images
		if plotfolder is None: return
		plot_dir = os.path.join(output_dir, plotfolder)
		os.makedirs(plot_dir, exist_ok=True)
		self.logger.info(f'Creating plots in: {plot_dir}')
		for metric, label in ApproachMetrics.metrics().items():
			fig = plots.linechart_approaches(
				df_comparison_pop, metric_column=metric, metric_label=label,
				figure_title=None
			)
			fig.savefig(os.path.join(plot_dir, f'approaches-populated-{metric}.png'), bbox_inches='tight')
			fig.savefig(os.path.join(plot_dir, f'approaches-populated-{metric}.pdf'), bbox_inches='tight')
			plots.cleanup(fig)

		# export additional plot with results by Romero et al.
		self.__plot_romero(df_comparison_pop, plot_dir)

	def __compare_target(self, dframes, target_fields, output_dir, out_name, pop_max: bool, plotfolder: str):
		""" Compare approaches w.r.t target field. """
		data = []
		for field_name in sorted(target_fields):
			for df, label in dframes:
				df_target = df[df[AnalysisKeys.TARGET_FIELD] == field_name]
				if pop_max:
					# limit selection to rows where #populated == populated_max
					df_target = df_target[
						df_target[AnalysisKeys.POPULATED_FIELDS_COUNT] == df_target[AnalysisKeys.POPULATED_FIELDS_MAX]
					]
				df_metrics = ApproachMetrics(df_target)
				row = {'target': field_name}
				row.update(self.__make_approach_row(label, df_metrics))
				data.append(row)
		df_comparison_tf = pd.DataFrame(data)
		df_comparison_tf.to_csv(os.path.join(output_dir, out_name), index=False, float_format='%.6f')
		self.logger.info(f'File created: {out_name}')
		if self.debug_print: print(df_comparison_tf)

		# plot and export images
		if plotfolder is None: return
		plot_dir = os.path.join(output_dir, plotfolder)
		os.makedirs(plot_dir, exist_ok=True)
		self.logger.info(f'Creating plots in: {plot_dir}')
		for metric, label in ApproachMetrics.metrics().items():
			fig = plots.barchart_approaches_target(
				df_comparison_tf, approach_column='approach', target_column='target',
				metric_column=metric, metric_label=label, x_ticks_rotation=30,
				figure_title=None
			)
			fig.savefig(os.path.join(plot_dir, f'approaches-target-{metric}.png'), bbox_inches='tight')
			fig.savefig(os.path.join(plot_dir, f'approaches-target-{metric}.pdf'), bbox_inches='tight')
			plots.cleanup(fig)

	@staticmethod
	def __plot_romero(df_comparison_pop: pd.DataFrame, plot_dir):
		""" Export additional plot with results by Romero et al. 2019 (NCBI/NCBI, Ontology-Based). """
		metric = 'mrr5'
		romero_cvr_data = pd.DataFrame.from_dict({
			'populated': list(range(1, 5)),
			'approach': ['CVR (Romero et al.)' for _ in range(1, 5)],
			metric: [0.54, 0.70, 0.79, 0.81]
		})
		romero_mffv_data = pd.DataFrame.from_dict({
			'populated': list(range(1, 5)),
			'approach': ['MFFV (Romero et al.)' for _ in range(1, 5)],
			metric: [0.38, 0.36, 0.30, 0.27]
		})
		romero_data = pd.concat([romero_cvr_data, romero_mffv_data], axis=0)
		df_combined = pd.concat([df_comparison_pop, romero_data], axis=0, join='outer', ignore_index=True)
		fig = plots.linechart_approaches(
			df_combined, metric_column=metric, metric_label=ApproachMetrics.metric_label(metric, 'MRR@5'),
			figure_title=None
		)
		fig.savefig(os.path.join(plot_dir, f'approaches-populated-{metric}-cvr.png'), bbox_inches='tight')
		fig.savefig(os.path.join(plot_dir, f'approaches-populated-{metric}-cvr.pdf'), bbox_inches='tight')
		plots.cleanup(fig)
