# This file contains constants used by scripts in this directory.


# Path to input test instances
INSTANCES_PATH = '../data/cedar/test-instances/ncbi_instances_formatted.jsonl'
INSTANCES_MAX = -1  # set -1 for unlimited or change from command line using -imax

# Path to URI mappings (json file with URI as key and for each a list of URIs)
URI_MAPPINGS_PATH = '../data/cedar/test-instances/mappings_merged.json'
USE_URI_MAPPINGS = True

# Path to JSON file containing top 10 most frequent values per form field
FREQUENT_VALUES_PATH = '../data/cedar/frequent-values/ncbi_annotated_frequent_values.json'

# API endpoints to use
ARSR_ENDPOINT = 'http://localhost:8050/suggest'
CVR_ENDPOINT = 'http://localhost:8050/suggest/cedar'


# Aggregation methods to extract from ARSR results.
# Full list here: suggestion_service/suggestion/scoring/aggregation.py
FIELD_AGGREGATION_METHODS = {
	'median', 'mean', 'min', 'max', 'product', 'random'
}
SCORE_AGGREGATION_METHODS = {
	'median', 'mean', 'min', 'max', 'product', 'random'
}


# Ontology prefixes to use in output files to decrease file size.
# Just make this dict empty to disable using prefixes.
# Note that obo already covers: PATO, UBERON, CL, BTO.
# Note: Use same for collect and analysis or URI-mappings won't work properly.
ONTO_PREFIXES = {
	'http://purl.obolibrary.org/obo/': 'obo',
	'http://www.ebi.ac.uk/efo/': 'efo',
	'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#': 'ncit'
}

# Separator between prefix and rest of the URI.
# (e.g. for ":" http://www.ebi.ac.uk/efo/example = efo:example)
# Note: Use same for collect and analysis or URI-mappings won't work properly.
PREFIX_SEPARATOR = ':'


# Path to- and name of directory for result files
OUTPUT_DIRECTORY = 'output/'

# Timestamp used to name result folders
OUTPUT_TIMESTAMP_FORMAT = '%Y%m%d-%H%M'

# Enable/disable using gzip to compress files during collection
OUTPUT_COMPRESS = True

# Only store top n recommendations during collection and discard the rest.
# Set this value to -1 to allow all (default: 40).
MAX_RECOMMENDATIONS = 40


#############################
# Only used by analyse.py:

# Add populated fields (and their values to the export).
EXPORT_POPULATED_FIELDS = True
EXPORT_POPULATED_VALUES = False

# Export at max this many recommendations to result files
EXPORT_RECOMMENDATIONS_TOP_N = 5
