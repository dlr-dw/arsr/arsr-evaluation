# This file contains methods to plot results.

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import axes, figure
import seaborn as sb


FONTSIZE_TITLE = 16
FONTSIZE_AXES = 14
FONTSIZE_LEGEND = 11
FONTSIZE_BAR_LABELS = 12
FONTSIZE_TICKS = 12


def cleanup(fig: figure.Figure):
	""" Cleanup last figure. """
	plt.close(fig)


def format_approach(name: str):
	""" Format approach names for use as labels. """
	if name.lower().startswith('arsr'):
		return name.replace(' ', '\n')
	return name


def barchart_approaches(
	df: pd.DataFrame, approach_column='approach', figure_title='MRR per Approach',
	metric_column='mrr5', metric_label='MRR@5', stylesheet='seaborn-paper',
	bar_color=None, label_color='#333', show_grid=True, show_legend=False):
	""" Plot bar chart for approach comparison.
	For colors see: https://matplotlib.org/stable/gallery/color/named_colors.html
	"""

	# prepare data
	approach_names = df[approach_column].apply(format_approach)  # format name labels
	approach_count = len(approach_names)
	x_ticks = range(approach_count)
	y_ticks = [i/10 for i in range(11)]
	# y_ticks = [0, 0.25, 0.5, 0.75, 1]

	# set stylesheet and prepare figure
	plt.style.use(stylesheet)
	fig: figure.Figure  # type hint for pycharm to work properly
	ax: axes.Axes  # also type hint
	fig, ax = plt.subplots(figsize=(6, 6))

	# get colors of stylesheet
	if bar_color is None:
		bar_color = plt.rcParams['axes.prop_cycle'].by_key()['color']

	# plot the actual bars
	plt.bar(
		x=x_ticks, height=df[metric_column], width=0.6,
		label=metric_label, color=bar_color
	)

	# plot background grid lines
	if show_grid:
		ax.grid(visible=True, axis='y', color='lightgray', linestyle='dotted', dashes=(5, 5))
		ax.set_axisbelow(True)

	# set axis ticks and labels
	ax.tick_params(labelsize=FONTSIZE_TICKS)
	plt.xticks(ticks=x_ticks, labels=approach_names, color='black')
	plt.yticks(ticks=y_ticks, color='black')

	# set fixed y-limits (add a bit of space above last tick)
	plt.ylim(top=max(y_ticks)+0.05)

	# title and axis labels
	if figure_title is not None: plt.title(figure_title, pad=10, fontsize=FONTSIZE_TITLE)
	plt.xlabel('Approach', labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)
	plt.ylabel(metric_label, labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)

	# enable legend (only useful for multiple values)
	if show_legend: plt.legend(fontsize=FONTSIZE_LEGEND)

	# set axes ratio
	ax.set_box_aspect(1)

	# add labels above bars
	bar_container = ax.containers[0]
	ax.bar_label(bar_container, labels=df[metric_column].round(4), padding=2, fontsize=FONTSIZE_BAR_LABELS)

	# set axis spine colors
	# for spine in ax.spines: ax.spines[spine].set_color(spine_color)

	# hide specific spines
	# ax.spines['top'].set_visible(False)
	# ax.spines['right'].set_visible(False)

	# adjust the padding between and around subplots
	fig.tight_layout()
	return fig


def linechart_approaches(
	df: pd.DataFrame, populated_column='populated', approach_column='approach',
	figure_title=None, metric_column='mrr5', metric_label='MRR@5', stylesheet='seaborn-paper',
	label_color='#333', show_grid=True, show_legend=True, legend_pos='upper left'):
	""" Compare approaches w.r.t no. populated fields. """

	y_ticks = [i / 10 for i in range(11)]
	populated_nums = df[populated_column].unique()

	# set stylesheet and prepare figure
	plt.style.use(stylesheet)
	fig: figure.Figure  # for pycharm to work
	ax: axes.Axes
	fig, ax = plt.subplots(figsize=(6, 6))

	# plot background grid lines
	if show_grid:
		ax.grid(visible=True, axis='both', color='lightgray', linestyle='solid')
		ax.set_axisbelow(True)

	# get default color cycle
	color_cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
	i = 0  # for counting colors
	contains_romero = False  # used to adjust the legend accordingly

	# plot one line per approach with specific styling
	approaches = set(df[approach_column])

	for a in sorted(approaches):

		linestyle = 'solid'
		linewidth = 2
		marker = ('.', 10)
		alpha = 1
		alow = a.lower()
		if alow.startswith('arsr-resim'):
			marker = ('D', 5)
		elif alow.startswith('arsr-simple'):
			pass
		elif alow.startswith('cvr'):
			linestyle = 'dashed'
		elif alow.startswith('mffv'):
			linestyle = 'dotted'

		# use fixed color for additional results by romero
		if 'romero' in alow:
			color = 'gray'
			marker = ('X', 8)
			alpha = 0.7
			contains_romero = True
		else:
			color = color_cycle[i % len(color_cycle)]
			i += 1

		plt.plot(
			populated_column, metric_column, data=df[df[approach_column] == a], label=a,
			marker=marker[0], markersize=marker[1], linestyle=linestyle, linewidth=linewidth,
			c=color, alpha=alpha
		)

	# set title, axis ticks and labels
	if figure_title is not None: plt.title(figure_title, pad=10, fontsize=FONTSIZE_TITLE)
	ax.tick_params(labelsize=FONTSIZE_TICKS)
	plt.xticks(populated_nums)
	plt.yticks(y_ticks)
	plt.xlabel('No. Populated Fields', labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)
	plt.ylabel(metric_label, labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)

	# set fixed y-limits (add a bit of space above last tick)
	plt.ylim(top=max(y_ticks)+0.05)

	if show_legend:
		ncol = 2 if contains_romero else 1
		lmode = 'expand' if contains_romero else None
		plt.legend(loc=legend_pos, fontsize=FONTSIZE_LEGEND, ncol=ncol, mode=lmode)
	ax.set_box_aspect(1)  # scale 1x1 aspect

	# adjust the padding between and around subplots
	fig.tight_layout()
	return fig


def barchart_approaches_target(
	df: pd.DataFrame, approach_column='approach', target_column='target', figure_title='MRR per Target Field',
	metric_column='mrr5', metric_label='MRR@5', stylesheet='seaborn-paper', label_color='#333',
	show_grid=True, show_legend=True, legend_pos='upper left', x_ticks_rotation=None):
	""" Plot bar chart for approach comparison.
	For colors see: https://matplotlib.org/stable/gallery/color/named_colors.html
	"""

	# prepare data
	target_fields = df[target_column].unique()
	target_field_count = len(target_fields)
	x_ticks = range(target_field_count)
	y_ticks = [i/10 for i in range(11)]
	# y_ticks = [0, 0.25, 0.5, 0.75, 1]

	# set stylesheet and prepare figure
	plt.style.use(stylesheet)
	fig: figure.Figure  # type hint for pycharm to work properly
	ax: axes.Axes  # also type hint
	fig, ax = plt.subplots(figsize=(6, 6))

	# create pivot table from data for plotting value of each approach for each target field
	pt = pd.pivot_table(df, values=metric_column, index=target_column, columns=approach_column, aggfunc='mean')
	pt.plot(ax=ax, kind='bar', legend=show_legend, width=0.75)

	# plot horizontal background grid lines
	if show_grid:
		ax.grid(visible=True, axis='y', color='lightgray', linestyle='dotted', dashes=(5, 5))
		ax.set_axisbelow(True)

	# set axis ticks and labels
	ax.tick_params(labelsize=FONTSIZE_TICKS)
	plt.xticks(ticks=x_ticks, labels=target_fields, color='black')
	plt.yticks(ticks=y_ticks, color='black')
	if x_ticks_rotation is not None: plt.xticks(rotation=x_ticks_rotation)

	# set fixed y-limits (add a bit of space above last tick)
	plt.ylim(top=max(y_ticks)+0.05)

	# title and axis labels
	if figure_title is not None: plt.title(figure_title)
	plt.xlabel('Target Field', labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)
	plt.ylabel(metric_label, labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)

	if show_legend: plt.legend(loc=legend_pos, fontsize=FONTSIZE_LEGEND)  # add legend
	ax.set_box_aspect(1)  # scale 1x1 aspect

	fig.tight_layout()
	return fig


def heatmap_arsr(
	df: pd.DataFrame, x='fam', y='sam', x_label='Field Aggregation Method', y_label='Score Aggregation Method',
	metric_column='mrr5', metric_label='MRR@5', figure_title='ARSR-Resim MRR per Aggregation Method',
	stylesheet='seaborn-paper', label_color='#333', num_format='.3g'):
	""" Compare ARSR variants using a heatmap created with seaborn. """

	plt.style.use(stylesheet)
	piv = df.pivot(index=x, columns=y, values=metric_column)  # build matrix
	ax: axes.Axes = sb.heatmap(
		piv, cmap='Blues', square=True, annot=True, annot_kws={'fontsize': 8},
		fmt=num_format, cbar_kws={'pad': 0.02}  # pad default 0.05 for vertical
	)
	ax.invert_yaxis()  # invert for better readability

	# set colorbar label
	ax_cb = ax.collections[0].colorbar
	ax_cb.set_label(metric_label, labelpad=10, color=label_color)

	# title and axis labels (inverted)
	if figure_title is not None: ax.set_title(figure_title, pad=10)
	ax.set_xlabel(y_label, labelpad=10, color=label_color)
	ax.set_ylabel(x_label, labelpad=10, color=label_color)

	fig = ax.get_figure()
	fig.tight_layout()
	return fig


def barchart_simple(
	df: pd.DataFrame, x_column, y_column, x_label, y_label, figure_title=None, stylesheet='seaborn-paper',
	bar_color=None, spine_color='black', label_color='#333', show_grid=True, x_ticks_rotation=None):
	""" Plots a simple bar chart (e.g. for frequency of #populated) and returns the figure. """

	# note: sorting not mandatory as x-axis of bar chart is always sorted
	df_sorted = df.sort_values(by=x_column)
	x_values = df_sorted[x_column].unique()
	populated_freq = df_sorted[y_column]
	x_ticks = x_values

	# set stylesheet and prepare figure
	plt.style.use(stylesheet)
	fig: figure.Figure  # for pycharm to work
	ax: axes.Axes
	fig, ax = plt.subplots(figsize=(6, 6))

	# plot the actual bars
	plt.bar(x=x_values, height=populated_freq, width=0.75, color=bar_color)

	# plot background grid lines
	if show_grid:
		ax.grid(visible=True, axis='y', color='lightgray', linestyle='dotted', dashes=(5, 5))
		ax.set_axisbelow(True)

	# set axis ticks and labels
	ax.tick_params(labelsize=FONTSIZE_TICKS)
	if x_ticks_rotation is None: plt.xticks(ticks=x_ticks, color='black')
	else: plt.xticks(ticks=x_ticks, color='black', rotation=x_ticks_rotation)

	# title and axis labels
	if figure_title is not None: plt.title(figure_title, pad=10, fontsize=FONTSIZE_TITLE)
	plt.xlabel(x_label, labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)
	plt.ylabel(y_label, labelpad=10, color=label_color, fontsize=FONTSIZE_AXES)

	# set fixed ratio of axes (1:1)
	ax.set_box_aspect(1)

	# add labels above bars
	bar_container = ax.containers[0]
	ax.bar_label(bar_container, labels=populated_freq, padding=2, fontsize=FONTSIZE_BAR_LABELS)

	# set axis spine colors
	for spine in ax.spines:
		ax.spines[spine].set_color(spine_color)

	# adjust the padding between and around subplots
	fig.tight_layout()
	return fig
