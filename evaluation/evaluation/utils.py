# Contains helper methods used for measurement.
import pathlib
import typing

import requests  # installation: pip install requests

from . import constants


def get_request_session():
	""" Returns a new HTTP session for API requests. """
	return requests.Session()


def create_dirs(path: str, log_func: typing.Callable = None):
	""" Create directory and subdirectories if missing and returns the Path instance. """
	path = pathlib.Path(path)
	if path.exists(): return path
	if log_func is not None:
		log_func(f'Creating directory: {path}')
	path.mkdir(parents=True, exist_ok=True)
	return path


def request_suse_api(endpoint: str, json_body: dict, session: requests.Session = None):
	""" Sends request at Suggestion Service API and returns suggestions. """
	if session is not None:
		r = session.post(endpoint, json=json_body)
	else:
		r = requests.post(endpoint, json=json_body)
	# print(f'Sending body: {json_body}')
	r.raise_for_status()  # raise exception for bad status codes
	jdata = r.json()  # raises exception on failure
	return jdata


def get_arsr_recommendation(target_field_name: str, populated_fields: dict, session=None):
	""" Get recommendations from association rules and semantic relatedness pipeline. """
	suggestions = request_suse_api(constants.ARSR_ENDPOINT, {
		'populated_fields': populated_fields,
		'target_field': target_field_name
	}, session=session)
	# TODO: maybe check that format is correct
	return suggestions


def get_cedar_recommendation(target_field_name: str, populated_fields: dict, session=None):
	""" Get recommendations from Cedar Value Recommender approach. """
	suggestions = request_suse_api(constants.CVR_ENDPOINT, {
		'populated_fields': populated_fields,
		'target_field': target_field_name
	}, session=session)
	# TODO: maybe check that format is correct
	return suggestions


def is_uri(value: str):
	""" Checks if a given value is a term-URI. """
	return value.lower().lstrip().startswith('http')


def same_in_mappings(uri1: str, uri2: str, uri_mappings: dict):
	""" Check if a mapping for these URIs exist. """
	m1 = uri_mappings.get(uri1, [])
	m2 = uri_mappings.get(uri2, [])
	return uri1 in m2 or uri2 in m1


def get_matching_score(expected_value: str, recommended_value: str, uri_mappings: dict = None, enforce_uris=True):
	""" Get a score (0 or 1) for the match of these values. """
	if expected_value == recommended_value: return 1

	# use "mappings" of term URIs as CVR did to check if both denote the same concept
	both_uris = is_uri(expected_value) and is_uri(recommended_value)
	if uri_mappings is not None and (both_uris or enforce_uris is False):
		if same_in_mappings(expected_value, recommended_value, uri_mappings):
			# print(f'Found same concept: {expected_value} = {recommended_value}')
			return 1

	same = str(expected_value).casefold() == str(recommended_value).casefold()
	return same


def get_expected_index(expected_value, recommendation: list, uri_mappings: dict = None):
	""" Get index of expected value in sorted recommendations list or -1 if not present. """
	for i in range(len(recommendation)):
		recommended_value = recommendation[i].get('value')
		if get_matching_score(expected_value, recommended_value, uri_mappings) > 0:
			return i
	return -1


def reciprocal_rank(expected_value_position: int, top_n: int):
	""" Compute reciprocal rank of expected value in top_n recommendations. """
	if 0 < expected_value_position <= top_n:
		return 1.0 / expected_value_position
	return 0.0


def compress_uri(value, prefixes: dict = None, prefix_separator: str = None):
	""" To convert/format the suggested value before using it.
	For instance, this replaces the proper parts of a URI with a prefix.
	If prefixes or separator are not defined, default values from constants are used.

	Parameters
	----------
	value : str | list
		A single value or a list of values.
	"""
	if prefixes is None: prefixes = constants.ONTO_PREFIXES
	if prefix_separator is None: prefix_separator = constants.PREFIX_SEPARATOR

	if isinstance(value, list):
		return [compress_uri(str(val), prefixes, prefix_separator) for val in value]

	if not isinstance(value, str): value = str(value)
	if value.lower().startswith('http'):
		# replace start of value URI with prefix
		for uri_start in prefixes:
			if value.startswith(uri_start):
				prefix = prefixes[uri_start]
				value_rest = value[len(uri_start):]
				if len(value_rest) == 0: continue  # uri is malformed
				value = f'{prefix}{prefix_separator}{value_rest}'
	return value


def convert_arsr_recommendation(suggestions, field_agg_methods, score_agg_methods, discard_zero=True):
	""" Converts recommendations of our suggestion service.

	The input format for field "suggestions" is currently:\n
	{
		"<suggested_value>": {
			"<field_aggregation_method>": {
				"<score_aggregation_method>": 0.123
			}
		}
	}

	This method creates a sorted list of suggestions for every possible combination.

	Parameters
	----------
	suggestions : dict
	field_agg_methods : typing.Collection[str]
		List of names of field (rule) aggregation methods to extract.
	score_agg_methods : typing.Collection[str]
		List of names of score (final) aggregation methods to extract.
	discard_zero : bool
		Discard recommendations with score of or below zero (default behaviour).

	Returns
	-------
	dict[tuple[str,str], list[dict]]
		A dictionary with combination (fam, sam) tuple as key and the recommendation list as value.
		Each recommendation list contains a dict per suggestion with value and score.
	"""
	# Note: "No suggestions" are allowed as well as it as a valid result
	if suggestions is None: suggestions = {}
	recommendation_lists = {}
	method_combinations = ((fam, sam) for fam in field_agg_methods for sam in score_agg_methods)

	for fam, sam in method_combinations:
		recommendations = []

		# get respective score for each recommended value
		for value, fam_scoring in suggestions.items():
			if fam not in fam_scoring: continue
			sam_scoring = fam_scoring[fam]
			if sam not in sam_scoring: continue
			score = sam_scoring[sam]
			if discard_zero and score <= 0: continue
			recommendations.append({
				'value': value,
				'score': score
			})

		# sort recommendation list by score and convert
		# to format: {"value": "<val>", "score": "<score>"}
		recommendations.sort(key=lambda e: e['score'], reverse=True)
		recommendation_lists[(fam, sam)] = recommendations

	return recommendation_lists


def convert_cvr_recommendation(suggestions, discard_zero=True):
	""" Converts CVR recommendations in same format as ARSR.

	The input format for field "suggestions" is currently:\n
	{ "<suggested_value>": 0.123 }

	Parameters
	----------
	suggestions : dict
	discard_zero : bool
		Discard recommendations with score of or below zero (default behaviour).

	Returns
	-------
	list[dict]
		A list of recommendations, each in a dict with value and score.
	"""
	if suggestions is None: suggestions = {}
	recommendations = []
	for value, score in suggestions.items():
		if discard_zero and score <= 0: continue
		recommendations.append({
			'value': value,
			'score': score
		})
	recommendations.sort(key=lambda e: e['score'], reverse=True)
	return recommendations


def count_same_score(scores, precision=-1):
	""" Get how many recommendations have the same score.

	Parameters
	----------
	scores : typing.Iterable
		Iterable of scores.
	precision : int | None
		Rounding precision applied so that close scores count as the same.
		Set this to a negative number to disable.
	"""
	score_counts = dict()  # key = score, value = counts
	for score in scores:
		if precision >= 0: score = round(score, precision)
		if score in score_counts: score_counts[score] += 1
		else: score_counts[score] = 1

	scores_same = 0
	for count in score_counts.values():
		if count > 1: scores_same += count
	return scores_same


def count_score(score, scores, precision=-1):
	""" Count how often `score` occurs in `scores`.

	Parameters
	----------
	score : float
	scores : typing.Iterable
	precision : int | None
		Rounding precision applied so that close scores count as the same.
		Set this to a negative number to disable.
	"""
	scores_same = 0
	if precision >= 0: score = round(score, precision)
	for s in scores:
		if precision >= 0: s = round(s, precision)
		if s == score: scores_same += 1
	return scores_same


def unique_before(recommendation: list, index: int, uri_mappings: dict = None, enforce_uris: bool = True):
	""" Get number of unique items in recommendation list before the given index.
	Note: The algorithm modifies the subset (recommendation[:index]) and removes all duplicates from it.
	I.e. the initial instance of recommendation could be altered when using a custom data structure for it.
	"""
	if index < 0: return 0
	items = recommendation[:index]
	if len(items) == 0: return 0
	elif len(items) == 1: return 1

	# iterate from left to right and pick item1
	i = 0
	while i < len(items):
		item1 = items[i]
		# until pos of item1, iterate from right to left, pick item2 and compare
		for k in reversed(range(i+1, len(items))):
			item2 = items[k]
			# if item2 matches item1, remove item2 from list
			if get_matching_score(item1, item2, uri_mappings, enforce_uris=enforce_uris) > 0:
				del items[k]
		i += 1
	return len(items)


def get_expected_rank_corrected(expected_value, recommendations: list, uri_mappings: dict = None, precision: int = -1):
	""" Get corrected rank of expected value in sorted recommendations list or 0 if not present.
	Returns a tuple where [0] is a rank order like this: 1, 1, 3, 3, 3, 6, 6, 7
	and where [1] is another order variant like this: 1, 1, 2, 2, 2, 3, 3, 4.
	"""
	pos = 0
	rank = 0
	pos_rank = 0
	last_score = -1
	for rec in recommendations:
		pos += 1
		score = rec['score']
		if precision >= 0:
			score = round(score, precision)
		if score != last_score:
			last_score = score
			rank += 1
			pos_rank = pos
		recommended_value = rec['value']
		if get_matching_score(expected_value, recommended_value, uri_mappings) > 0:
			return pos_rank, rank
	return 0, 0
