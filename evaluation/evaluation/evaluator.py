import itertools
import json
import logging
import os.path
import pathlib
import time
from collections import deque

from .measurement import Measurement


class Evaluator:
	""" Class to perform the actual evaluation. """

	def __init__(self, measurement: Measurement):
		self.logger = logging.getLogger(__name__)
		self.measurement = measurement
		self._instance_num = 0
		self._instances_skipped = 0
		self.shutdown = False

	@property
	def instances_processed(self):
		return self._instance_num

	@property
	def instances_skipped(self):
		return self._instances_skipped

	def abort(self):
		""" Notify that evaluation should be stopped / aborted.
		This will gracefully stop a running evaluation.
		"""
		self.shutdown = True
		self.measurement.abort()

	def start_evaluation(self, instances_path: str, start_from: int, instances_max: int):
		""" Start the collection step of the evaluation.
		This step retrieves instances and prepares them for making suggestions.
		"""
		progress_last = -1
		filename_last = None
		self._instance_num = 0
		self._instances_skipped = 0
		instances_limited = instances_max >= 0
		start_skip = max(0, start_from - 1)

		# store last n timings for estimation of remaining time
		timings_last = deque(maxlen=100)

		for instance, filename, line_num, lines_total in self._get_test_instances(instances_path):

			if self.shutdown:
				self.logger.warning('Evaluation aborted.')
				break

			# detect file change for progress log
			if filename != filename_last:
				filename_last = filename
				progress_last = -1
				self.logger.info(f'Processing file: {filename}')

			# skip first n instances
			if start_skip > 0:
				start_skip -= 1
				continue
			elif start_skip == 0:
				start_skip = -1
				self.logger.info(f'Skipped first {start_from - 1} instances.')

			# log current progress
			pinf = ''
			if instances_limited and instances_max < lines_total:
				progress = round(self._instance_num / instances_max, 2)
				instances_remaining = instances_max - self._instance_num
			else:
				progress = round((line_num - 1) / lines_total, 2)
				instances_remaining = lines_total - line_num + 1
				pinf = ' in file'

			if progress != progress_last:
				progress_last = progress
				if progress > 0: self.logger.info(f'Progress{pinf}: {int(progress * 100)}%')
				timings_total = len(timings_last)
				if timings_total > 0:  # prevent division by zero and early logging
					avg_runtime = sum(timings_last) / timings_total
					self.logger.info(f'Average runtime (last {timings_total}): {avg_runtime}')
					self.logger.info(f'Estimated remaining runtime: {avg_runtime * instances_remaining}')

			# check if instances limit reached
			if instances_limited and self._instance_num >= instances_max:
				self.logger.warning(f'Abort. Reached maximum number of instances ({instances_max}).')
				break

			# prepare instance for processing
			time_start = time.perf_counter()
			self._instance_num += 1  # increase here as it would otherwise be hard to continue at a specific num
			instance_num_total = self._instance_num + start_from - 1
			instance_id = instance['id']
			field_names = self._get_populated_field_names(instance['fields'])
			num_populated = len(field_names)
			self.measurement.set_instance(instance, instance_num_total, instance_id, num_populated)

			# non-empty fields only and at least two are required (one target, one populated)
			if num_populated < 2:
				self.logger.info(
					f'Skipping instance {instance_num_total} (id: {instance_id}) - too few fields: {num_populated}')
				self._instances_skipped += 1
				continue

			# get total number of combinations created by the following for loops
			n = len(field_names)
			num_combis = n * (2 ** (n - 1) - 1)
			self.logger.info(f'Instance {instance_num_total} ({instance_id}), request combinations: {num_combis}')

			# test each field once as the target field
			for target_field_name in sorted(field_names):
				target_field = self.__get_field_info(instance, target_field_name)

				# get a copy of field names without current target field (note: field_names must be a set)
				other_fields = field_names - {target_field_name}

				# generate every combination of populated fields for every size
				field_combinations = []
				for populated_size in range(1, len(other_fields) + 1):
					combi = itertools.combinations(sorted(other_fields), populated_size)
					field_combinations.extend(combi)

				# test target field with each combination and write results to file
				for populated_field_names in field_combinations:
					populated_fields = dict()

					# Get populated fields information.
					# Expected format: {"fieldname": "value"} or {"fieldname": ["value1", "value2", ...]}
					# (format defined by: suggestion_service.service.SuggestionService.generate_suggestions)
					for field_name in populated_field_names:
						field = self.__get_field_info(instance, field_name)
						field_value = self.__get_field_value(field)
						if field_value is None:
							self.logger.warning(f'(populated_fields) Skipping field without value: {field_name}')
							continue
						populated_fields[field_name] = field_value

					# Get expected value(s) using value_type (or value_label)
					expected_value = target_field.get('value_type')
					if expected_value is None:
						expected_value = target_field.get('value_label')
						if expected_value is None:
							self.logger.warning(f'No value(s) for "expected" from target field: {target_field_name}')
							break  # discard working with this target field
						self.logger.warning(f'Target field without type: {target_field_name} - using label instead.')

					# perform recommendation and store results
					self.measurement.measure(target_field_name, populated_fields, expected_value)
					if self.shutdown: break
				if self.shutdown: break

			runtime_sec = time.perf_counter() - time_start
			timings_last.append(runtime_sec)

	def _get_test_instances(self, instances_path):
		""" Loads test instances from a JSONL file (one instance per line).
		Returns a generator of (instance, instance_num, instances_total) tuples.
		"""
		path = pathlib.Path(instances_path)
		if not path.exists() or not path.is_file():
			raise IOError('instances file invalid')

		lines_total = 0
		with path.open('r') as fin:
			for _ in fin:
				lines_total += 1
		self.logger.info(f'Instances to load: {lines_total}')

		with path.open('r') as fin:
			filename = os.path.basename(fin.name)
			line_num = 0
			for line in fin:
				line_num += 1
				try:
					instance = json.loads(line)
					yield self._prepare_instance(instance), filename, line_num, lines_total
				except (json.JSONDecodeError, ValueError) as ex:
					self.logger.error(f'Failed loading instance from: {filename} (L: {line_num}) - {ex}')

	@staticmethod
	def _prepare_instance(instance):
		""" Checks if instance dict has the required fields and returns it.

		Note that currently, the focus is on single-value form fields because the evaluation
		is performed against the data of the Cedar VR, which consists of single-value fields only.
		Support for multi-value fields is only given in a very simple way, where either
		"value_label" or "value_type" must be a list of values.

		The single-value instance should have the following structure (source is optional):
		{
			"id": "61ecc0c0-775b-4978-b65e-47120f3cc41d",
			"source": "instances_110001to120000.jsonl",
			"fields": {
				"cell_line": {},
				"tissue": {
					"value_label": "BONE MARROW",
					"value_type": "http://purl.obolibrary.org/obo/UBERON_0002371",
					"field_type": "http://www.ebi.ac.uk/efo/EFO_0000635"
				},
				...
			}
		}
		"""
		required = ('id', 'fields')
		missing = []
		for req in required:
			if req not in instance:
				missing.append(req)
		if len(missing) > 0:
			missing_summary = ','.join(f'"{m}"' for m in missing)
			raise ValueError(f'missing {missing_summary}')
		return instance

	@classmethod
	def _get_populated_field_names(cls, instance_fields) -> set:
		""" Get names of populated fields only (require at least "value" to be set). """
		field_names = set()

		for field_name, field_data in instance_fields.items():
			if len(field_data) == 0: continue

			# check if value_type (URI) or at least label (text) is given
			v = cls.__get_field_value(field_data)
			if v is None: continue

			# skip v if it is an empty list
			if isinstance(v, list) and len(v) == 0: continue

			# assume v is just a single string value
			v = str(v).strip()
			if len(v) == 0: continue
			field_names.add(field_name)

		return field_names

	@staticmethod
	def __get_field_info(instance: dict, field_name: str):
		""" Get field information as dict from current instance. """
		return instance['fields'][field_name]

	@staticmethod
	def __get_field_value(field: dict):
		""" Returns the value type or the label if type is not given. """
		field_value = field.get('value_type')  # prefer type (URI)
		if field_value is None:  # otherwise, use label
			field_value = field.get('value_label')
		return field_value
