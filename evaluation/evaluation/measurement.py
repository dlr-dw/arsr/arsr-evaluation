import gzip
import json
import logging
import os.path
import pathlib
import time
import typing
from datetime import datetime, timezone

import requests.exceptions

from . import utils, constants


class ResultKeys:
	""" Keys used in exported file. """
	INSTANCE_ID = 'id'
	TARGET_FIELD_NAME = 'tf'
	POPULATED_FIELDS = 'pf'
	POPULATED_FIELDS_MAX = 'pfm'  # max no. pf in this instance (= #fields - 1 for target)
	EXPECTED_VALUE = 'ev'  # a single value or possibly also a list
	RECOMMENDATIONS = 'rec'  # list of rec. (can be limited in size)
	RECOMMENDATIONS_NUM = 'num'  # total num of recommendations
	RUNTIME = 'time'
	FAIL = 'fail'
	FAIL_REASON = 'reason'


class Measurement:
	""" Performs evaluation and stores results. """

	def __init__(self, output_dir, retries_max=5, retries_wait_sec=3, max_rec=None, compress=None, arsr_only=False):
		"""
		Parameters
		----------
		output_dir : str
			Output directory for result files. Should already exist.
		retries_max : int
			Max. no. retries for getting recommendations from endpoints.
		retries_wait_sec : int
			Time in seconds to wait after a failed request before issuing the next.
		max_rec : int | None
			Max. no. recommendations to store. Further won't be exported.
		compress : bool | None
			Compress (gzip) output files . If None, uses value defined in constants.
		arsr_only : bool
			Only run ARSR recommender and do not use any of the baselines.
		"""
		self.logger = logging.getLogger(__name__)
		self.output_dir = output_dir
		self.instance = None
		self.instance_num = 0
		self.instance_id = 0
		self.instance_populated_num = 0  # no. populated fields of this instance
		self.retries_max = retries_max
		self.retries_wait_sec = retries_wait_sec
		self.shutdown = False
		self.failures = {}
		self.arsr_only = arsr_only

		# max number of recommendations to store
		if max_rec is None: self.max_recommendations = constants.MAX_RECOMMENDATIONS
		else: self.max_recommendations = max_rec

		# compression of output files
		if compress is None: self.use_compression = constants.OUTPUT_COMPRESS
		else: self.use_compression = compress
		self.file_handles = dict()  # holds file handle (value) for file name (key)

		# use session object for persistent http connections
		self.http_session = utils.get_request_session()

		# used by most frequent field values baseline
		self.frequent_field_values = None
		self.frequent_field_values_notified = False

		self.logger.info(
			f'Measurement: compress={self.use_compression}, max_rec={self.max_recommendations}, '
			f'arsr_only={self.arsr_only}, retries_max={self.retries_max}'
		)

	@staticmethod
	def get_timestamp():
		return datetime.now(timezone.utc).strftime(constants.OUTPUT_TIMESTAMP_FORMAT)

	def get_failures(self):
		return self.failures

	def set_instance(self, instance, instance_num: int, instance_id, num_populated_fields: int):
		self.instance = instance
		self.instance_num = instance_num
		self.instance_id = instance_id
		self.instance_populated_num = num_populated_fields

	def load_frequent_field_values(self, path: str):
		""" Load most frequent field values from file. """
		with open(path, 'r') as fin: values = json.load(fin)
		if not isinstance(values, dict):
			raise ValueError('Invalid frequent values data.')
		for field, suggestions in values.items():
			if not isinstance(suggestions, list):
				raise ValueError(f'Invalid frequent values data in field {field}.')
			# format list of strings and add imaginary score based on rank
			formatted = []
			for i in range(len(suggestions)):
				formatted.append({
					'value': suggestions[i],
					'score': round(1 - (i / len(suggestions)), 2)
				})
			values[field] = formatted
		self.frequent_field_values = values
		self.logger.info('Most frequent field values loaded.')

	def __get_recommendation(self, approach_name: str, func: typing.Callable):
		""" Get API response for approach including retry and error handling. """
		response = {}  # API response
		runtime_seconds = 0
		retries = 0
		failed = False
		fail_reason = ''

		while retries <= self.retries_max:
			if self.shutdown: break
			if retries > 0: self.logger.info(f'Starting retry {retries}/{self.retries_max}')
			try:
				# TODO: maybe retrieve stats with request and store them as well
				t0 = time.perf_counter()
				response = func()
				runtime_seconds = time.perf_counter() - t0
				if not response.get('success', False):
					fail_reason = response.get('reason', 'unknown')
					raise Exception(f'Request unsuccessful, reason: {fail_reason}')
				self.logger.debug(f'{approach_name} done ({round(runtime_seconds, 4)} sec)')
				# print(f'\n{approach_name}:\n{recommendations}')
				break
			except Exception as ex:
				self.logger.exception(f'{approach_name} recommendation failed (retry {retries}/{self.retries_max}).')
				time.sleep(self.retries_wait_sec)  # wait before retrying
				fail_reason = str(ex)
				if isinstance(ex, requests.exceptions.ConnectionError):
					fail_reason = 'API connection failed'
				retries += 1

		if self.shutdown:
			failed = True
			fail_reason = 'Abort'
		elif retries > self.retries_max:
			failed = True
			self.logger.warning(f'{approach_name} recommendation reached max retries.')

		if failed is True:
			if approach_name not in self.failures:
				self.failures[approach_name] = 0
			self.failures[approach_name] += 1

		if response is None: response = {}
		return {
			'response': response,  # API response
			'runtime': runtime_seconds,
			'retries': retries,
			'failed': failed,
			'reason': fail_reason
		}

	def abort(self):
		""" For gracefully stop of a running evaluation. """
		self.shutdown = True

	def measure(self, target_field_name, populated_fields, expected):
		""" Runs recommendation for the target field, based on the populated fields.

		Parameters
		----------
		target_field_name : str
		populated_fields : dict
			A dictionary with key = field name and value = field value.
			A value must be assigned or otherwise the field is not considered populated.
		expected : str | list
			The expected value or list of values.
		"""
		self.logger.debug(f'Measuring: target={target_field_name}, populated={list(populated_fields.keys())}')
		if self.arsr_only is False:
			self._measure_mffv(target_field_name, populated_fields, expected)
			self._measure_cvr(target_field_name, populated_fields, expected)
		self._measure_arsr(target_field_name, populated_fields, expected)

	def _measure_arsr(self, target_field_name: str, populated_fields: dict, expected):
		""" Measure recommendations from our approach (ARSR - Association Rules and Semantic Relatedness). """
		self.logger.debug('Retrieving ARSR recommendation...')
		arsr_result = self.__get_recommendation('ARSR',
			lambda: utils.get_arsr_recommendation(target_field_name, populated_fields, self.http_session)
		)
		arsr_suggestions = arsr_result['response'].get('suggestions', None)
		arsr_time_seconds = arsr_result.get('runtime', -1)

		if arsr_suggestions is None or len(arsr_suggestions) == 0:
			self.logger.debug('ARSR delivered no suggestions.')

		# create every possible combination to sort the recommendations
		arsr_result_combinations = utils.convert_arsr_recommendation(
			arsr_suggestions,
			field_agg_methods=constants.FIELD_AGGREGATION_METHODS,
			score_agg_methods=constants.SCORE_AGGREGATION_METHODS
		)

		# gather ARSR results and write to file(s)
		self.logger.debug('Exporting ARSR result.')
		for method_combination, arsr_recommendation in arsr_result_combinations.items():
			fam = method_combination[0]  # field aggregation method
			sam = method_combination[1]  # score aggregation method
			data = self.__create_result_raw(
				target_field_name, populated_fields, expected, arsr_recommendation, arsr_time_seconds,
				arsr_result.get('failed', False), reason=arsr_result.get('reason', 'unknown')
			)
			self.__write_result_raw(data, f'arsr_{fam}-{sam}')

	def _measure_cvr(self, target_field_name: str, populated_fields: dict, expected):
		""" Measure recommendations from Cedar Value Recommender (CVR). """
		self.logger.debug('Retrieving CVR recommendation...')
		cedar_result = self.__get_recommendation('CVR',
			lambda: utils.get_cedar_recommendation(target_field_name, populated_fields, self.http_session)
		)
		cedar_suggestions = cedar_result['response'].get('suggestions', None)
		cedar_time_seconds = cedar_result.get('runtime', -1)

		if cedar_suggestions is None or len(cedar_suggestions) == 0:
			self.logger.debug('CVR delivered no suggestions.')

		# gather CVR result and write to file
		cvr_recommendation = utils.convert_cvr_recommendation(cedar_suggestions)
		self.logger.debug('Exporting CVR result.')
		data = self.__create_result_raw(
			target_field_name, populated_fields, expected, cvr_recommendation, cedar_time_seconds,
			cedar_result.get('failed', False), reason=cedar_result.get('reason', 'unknown')
		)
		self.__write_result_raw(data, 'cvr')

	def _measure_mffv(self, target_field_name: str, populated_fields: dict, expected):
		""" Measure recommendations using most frequent field values (MFFV) as another baseline. """
		if self.frequent_field_values is None:
			if self.frequent_field_values_notified is False:
				self.logger.warning('Skipping MFFV evaluation: most frequent field values are not set!')
				self.frequent_field_values_notified = True
			return

		self.logger.debug('Retrieving MFFV recommendation...')
		t0 = time.perf_counter()
		mffv_recommendation = self.frequent_field_values.get(target_field_name, [])
		mffv_time_seconds = time.perf_counter() - t0
		data = self.__create_result_raw(
			target_field_name, populated_fields, expected,
			mffv_recommendation, mffv_time_seconds, False, ''
		)
		self.__write_result_raw(data, 'mffv')

	def __create_result_raw(
		self, target: str, populated: dict, expected, recommendation, runtime_sec: float,
		failed: bool, reason: str):
		""" Creates the dictionary with the information to export.
		Note that expected could also be a list if multiple values were given!
		"""
		runtime_ms = round(runtime_sec * 1000, 3)

		# compress URIs using prefixes (use defaults defined in constants)
		comp_uri = utils.compress_uri
		populated_fields_out = {field: comp_uri(value) for field, value in populated.items()}
		expected_value_out = comp_uri(expected)  # note: could be a list!
		max_recs = len(recommendation) if self.max_recommendations < 0 else self.max_recommendations
		recommendations_out = [
			{'value': comp_uri(r['value']), 'score': r['score']}
			for r in recommendation[:max_recs]
		]

		# assemble output dict to store in file
		result = {
			ResultKeys.INSTANCE_ID: self.instance_id,
			ResultKeys.TARGET_FIELD_NAME: target,
			ResultKeys.POPULATED_FIELDS: populated_fields_out,
			ResultKeys.POPULATED_FIELDS_MAX: self.instance_populated_num-1,
			ResultKeys.EXPECTED_VALUE: expected_value_out,
			ResultKeys.RECOMMENDATIONS: recommendations_out,
			ResultKeys.RECOMMENDATIONS_NUM: len(recommendation),
			ResultKeys.RUNTIME: runtime_ms,
			ResultKeys.FAIL: failed
		}
		if failed: result[ResultKeys.FAIL_REASON] = reason
		return result

	def __write_result_raw(self, data: dict, filename: str):
		""" Write unprocessed recommendation result to file. """

		# try to create folder if necessary
		out_path = utils.create_dirs(self.output_dir, self.logger.info)
		if not out_path.is_dir(): raise Exception('Invalid output directory!')

		# assemble final file path
		filename += '.jsonl'
		if self.use_compression: filename += '.gz'
		path_combined = os.path.join(out_path, filename)
		path = pathlib.Path(path_combined)

		def write_out(json_data: dict, file_handle: typing.IO):
			self.logger.debug(f'Writing to: {os.path.basename(file_handle.name)}')
			json.dump(json_data, file_handle, indent=None)
			file_handle.write('\n')

		if self.use_compression:
			# store file handles and keep streams open when using compression
			path_str = str(path)
			file = self.file_handles.get(path_str, None)
			if file is None:
				file = gzip.open(path, 'at')  # requires at least python 3.6
				self.file_handles[path_str] = file
			write_out(data, file)
		else:
			# open, write and close right after finishing
			with path.open('a') as out:
				write_out(data, out)

	def cleanup(self):
		""" Free resources like closing opened file streams. """
		if len(self.file_handles) > 0:
			for file in self.file_handles.values():
				self.logger.info(f'Saving file: {os.path.basename(file.name)}')
				file.close()
		else:
			self.logger.info('No cleanup tasks.')
