import csv
import gzip
import json
import logging
import multiprocessing as mp
import os
import pathlib
import time
import typing

from . import utils, constants
from .measurement import ResultKeys


class AnalysisKeys:
	""" Keys used in exported file. """
	INSTANCE_ID = 'inst_id'
	TARGET_FIELD = 'target_field'
	EXPECTED_VALUE = 'expected_value'
	EXPECTED_VALUE_POS = 'ev_pos'
	EXPECTED_VALUE_POS_C1 = 'ev_pos_c1'  # corrected pos like [1, 1, 3, 3, 3, 6, 6]
	EXPECTED_VALUE_POS_C2 = 'ev_pos_c2'  # corrected pos like [1, 1, 2, 2, 2, 3, 3]
	POPULATED_FIELDS = 'populated_fields'
	POPULATED_FIELDS_COUNT = 'pf_count'
	POPULATED_FIELDS_MAX = 'pf_max'


class Analysis:
	""" Reads the recommendation results and performs an analysis. """

	def __init__(self, score_precision=-1, discard_duplicates=False, max_processes=1, group_arsr=None):
		"""
		Parameters
		----------
		score_precision : int
			Round scores at this many digits after comma or don't round if negative.
		discard_duplicates : bool
			Do not export (target_field, populated_fields, expected_value) combinations that already occurred.
		max_processes : int
			Max. number of processes to use for multiprocessing.
		group_arsr : str | None
			Set the name of the ARSR variant for grouping file starting with "arsr" in a folder.
			The folder name is then composed of "arsr_<group_arsr>". None disables grouping.
		"""
		self.logger = logging.getLogger(__name__)
		self.prec = score_precision  # precision for score rounding (-1 to disable)
		self.uri_mappings = None
		self.discard_duplicates = discard_duplicates

		if max_processes < 0: max_processes = 1
		elif max_processes > os.cpu_count() or max_processes == 0: max_processes = os.cpu_count()
		self.max_processes = max_processes

		if group_arsr is not None and len(group_arsr.strip()) == 0: group_arsr = None
		self.group_arsr = group_arsr

		self.logger.info(
			f'Analysis: score_precision={self.prec}, discard_duplicates={discard_duplicates}, '
			f'max_processes: {self.max_processes}, group_arsr={group_arsr}'
		)

	def load_uri_mappings(self, uri_mappings_path: str, compress_uris: bool = True):
		""" Load URI mappings from file. Returns True on success. """
		try:
			with open(uri_mappings_path, 'r') as fin:
				uri_mappings = json.load(fin)
			self.logger.info(f'URI mappings loaded: {len(uri_mappings)}')
			self.set_uri_mappings(uri_mappings, compress_uris=compress_uris)
		except Exception as ex:
			self.logger.error(f'Invalid URI mappings file: {ex}')
			return False
		return True

	def set_uri_mappings(self, uri_mappings: dict, compress_uris: bool = True):
		""" Set mappings used to identify term-URIs that mean the same.
		Expected is a dict with key = URI and value = list of URIs that mean the same.
		If compress_uris is True, uses defined ontology prefixes to compress.
		"""
		if compress_uris:
			compressed = dict()
			for uri, uris in uri_mappings.items():
				uri = utils.compress_uri(uri)
				compressed[uri] = [utils.compress_uri(u) for u in uris]
			self.uri_mappings = compressed
		else:
			self.uri_mappings = uri_mappings

	def analyse_dir(
		self, directory_path: str, output_dir: str, output_compressed: bool = False, max_depth: int = 1):
		""" Analyse all files in given directory and write results to output directory. """
		path = pathlib.Path(directory_path)
		if not path.is_dir():
			self.logger.error('Invalid directory path.')
			return

		filepaths = []
		curdepth = 1
		for dirpath, dirnames, filenames in os.walk(path):
			for filename in filenames:
				filepath = os.path.join(dirpath, filename)
				filepaths.append(filepath)
			if curdepth >= max_depth: break
			curdepth += 1
		self.logger.info(f'Files to analyse: {len(filepaths)}')

		# "single-threaded" processing
		if self.max_processes < 2:
			self.logger.info('Starting analysis in single-threaded mode.')
			for path in filepaths:
				self.analyse_file(path, output_dir, output_compressed)
			return

		# use multiprocessing
		self.logger.info(f'Using multiprocessing with {self.max_processes} processes.')
		first = True
		with mp.Pool(self.max_processes) as pool:
			for path in filepaths:
				pool.apply_async(
					self.analyse_file, (path, output_dir, output_compressed),
					callback=lambda e: self.logger.info(f'Finished analysing {e}'),
					error_callback=lambda e: self.logger.error(e)
				)
				# wait a moment for output dir to get created
				time.sleep(0.2 if first else 0.01)
				if first: first = False
			pool.close()
			pool.join()

	def analyse_file(self, filepath: str, output_dir: str, output_compressed: bool = False):
		""" Analyse a single file and write out results. Returns the file name. """
		try:
			if filepath.endswith('.gz'):  # open a compressed file
				with gzip.open(filepath, 'rt') as file:
					self._analyse_file(file, output_dir, output_compressed)
			else:  # open a generic file
				with open(filepath, 'r') as file:
					self._analyse_file(file, output_dir, output_compressed)
		except Exception as ex:
			raise type(ex)(f'Failed to analyse {filepath}')
		return filepath

	def _analyse_file(self, file_handle: typing.IO, output_dir: str, output_compressed: bool = False):
		""" Analyse results stored in the file and write it to the output directory. """
		basename = os.path.basename(file_handle.name)
		self.logger.info(f'Analysing: {basename}')

		# prepare output directory and file
		fn_split = os.path.splitext(basename)
		while fn_split[1] != '':  # remove file extension
			fn_split = os.path.splitext(fn_split[0])
		output_filename = fn_split[0]
		open_func, open_params = self.prepare_write(output_dir, output_filename, output_compressed)

		# open with newline='' to avoid additional line breaks of csv writer
		with open_func(*open_params, newline='') as file_out:
			w = csv.writer(file_out)
			combinations_exported = set()  # stores (tf, pf, ev) combinations for deduplication
			create_header = True
			lno = 0

			for line in file_handle:
				lno += 1
				try:
					jdata = json.loads(line)
					data_out, combination = self._analyse_entry(jdata)

					if self.discard_duplicates:
						# skip exporting this entry if it's a duplicate
						if combination in combinations_exported: continue
						combinations_exported.add(combination)

					if create_header:
						create_header = False
						w.writerow(e[0] for e in data_out)
					w.writerow(e[1] for e in data_out)

				except KeyError as ke:
					self.logger.error(f'Found invalid data in file: {file_out.name}')
					raise ValueError(f'Invalid data in line {lno}. Missing key: {ke}')
				except Exception:
					self.logger.error(f'Failed processing file: {file_out.name}')
					raise ValueError(f'Failed processing line {lno}')

	def _analyse_entry(self, jdata: dict):
		""" Analyse a single JSON entry and get the list of data tuples. """
		instance_id = jdata[ResultKeys.INSTANCE_ID]
		target_field = jdata[ResultKeys.TARGET_FIELD_NAME]
		populated = jdata[ResultKeys.POPULATED_FIELDS]
		populated_max = jdata[ResultKeys.POPULATED_FIELDS_MAX]  # max no. populated fields (per instance)
		recommendation = jdata[ResultKeys.RECOMMENDATIONS]
		recommendations_total = jdata[ResultKeys.RECOMMENDATIONS_NUM]
		expected_value = jdata[ResultKeys.EXPECTED_VALUE]
		runtime = jdata[ResultKeys.RUNTIME]
		fail = jdata.get(ResultKeys.FAIL, False)
		reason = jdata.get(ResultKeys.FAIL_REASON, 'unknown' if fail else '')

		# expected_value could be a list if multi-value fields are used
		# TODO: if required, implement support for multi-value fields
		if isinstance(expected_value, list):
			raise NotImplementedError('Analysis currently only supports a single value for expected.')

		# test combination for deduplication (also returned)
		combination = (str(target_field), str(populated), str(expected_value))

		# create data as list of tuples with [0] = column name and [1] = actual entry
		return self._analyse_result(
			instance_id=instance_id, target_field_name=target_field,
			populated_fields=populated, populated_fields_max=populated_max,
			expected_value=expected_value, recommendation=recommendation, recommendations_total=recommendations_total,
			runtime_ms=runtime, fail=fail, fail_reason=reason
		), combination

	def _analyse_result(
		self, instance_id: str, target_field_name: str, populated_fields: dict, populated_fields_max: int,
		expected_value: str, recommendation: typing.List[dict], recommendations_total: int,
		runtime_ms: float, fail: bool, fail_reason: str):
		""" Create a list of tuples of analysed result data. """

		# get index of expected value in recommendation list (zero-based)
		ev_index = utils.get_expected_index(expected_value, recommendation, self.uri_mappings)
		ev_pos = ev_index + 1  # used for reciprocal rank (one-based rank, 0 = missing)

		# get corrected position (all with same score get the same place)
		ev_corrected_rank_pos, ev_corrected_rank = utils.get_expected_rank_corrected(
			expected_value, recommendation, self.uri_mappings, self.prec)

		# get populated fields count and string representation
		populated_fields_count = len(populated_fields)
		populated_fields_str = None
		if constants.EXPORT_POPULATED_FIELDS and not constants.EXPORT_POPULATED_VALUES:
			populated_fields_str = [str(f) for f in populated_fields.keys()]
		elif constants.EXPORT_POPULATED_FIELDS and constants.EXPORT_POPULATED_VALUES:
			populated_fields_str = [f'{f}={v}' for f, v in populated_fields.items()]
		elif constants.EXPORT_POPULATED_VALUES:
			populated_fields_str = [str(v) for v in populated_fields.values()]

		# get top n recommendations as strings
		def round_score(score):
			if self.prec < 0: return score
			return round(score, self.prec)
		top_n = constants.EXPORT_RECOMMENDATIONS_TOP_N
		recommendation_str = [f'{r["value"]} ({round_score(r["score"])})' for r in recommendation[:top_n]]

		# get number of recommendations with same score
		num_same_score = utils.count_same_score((r['score'] for r in recommendation), precision=self.prec)
		num_same_score_top5 = utils.count_same_score((r['score'] for r in recommendation[:5]), precision=self.prec)
		num_same_score_top10 = utils.count_same_score((r['score'] for r in recommendation[:10]), precision=self.prec)

		# count how often the expected value occurs
		ev_score = 0
		ev_score_count = 0
		if ev_index >= 0:
			ev_score = recommendation[ev_index]['score']
			if self.prec >= 0: ev_score = round(ev_score, self.prec)
			ev_score_count = utils.count_score(ev_score, (r['score'] for r in recommendation), self.prec)

		# get number of unique suggestions before expected value
		unique_before_ev = utils.unique_before([r['value'] for r in recommendation], ev_index, self.uri_mappings)

		# prepare the result data (first entry in tuple is the header name)
		ak = AnalysisKeys
		data = [
			(ak.INSTANCE_ID, instance_id),
			(ak.TARGET_FIELD, target_field_name)
		]

		# add populated fields if desired
		if populated_fields_str is not None:
			data.append((ak.POPULATED_FIELDS, ', '.join(populated_fields_str)))

		data.extend([
			(ak.POPULATED_FIELDS_COUNT, populated_fields_count),
			(ak.POPULATED_FIELDS_MAX, populated_fields_max),
			(ak.EXPECTED_VALUE, expected_value),
			(ak.EXPECTED_VALUE_POS, ev_pos),
			(ak.EXPECTED_VALUE_POS_C1, ev_corrected_rank_pos),
			(ak.EXPECTED_VALUE_POS_C2, ev_corrected_rank),
			('ev_score', ev_score),
			('ev_score_count', ev_score_count),
			('unique_before_ev', unique_before_ev),
			('rec_total', recommendations_total),
			('runtime_ms', runtime_ms),
			('same_score', num_same_score),
			('same_score_top5', num_same_score_top5),
			('same_score_top10', num_same_score_top10),
			('fail', 1 if fail else 0),
			('fail_reason', fail_reason)
		])

		# add top recommendations
		top_num = constants.EXPORT_RECOMMENDATIONS_TOP_N
		if top_num > 0:
			data.append((f'top{top_num}_values', ', '.join(recommendation_str)))

		return data

	def prepare_write(self, output_dir: str, filename: str, compress=False):
		""" Prepares write directory and returns a tuple (open_func, open_params). """

		# assemble path and create output directory if required
		filename += '.csv'
		if compress: filename += '.gz'
		if self.group_arsr is not None and filename.lower().startswith('arsr'):
			output_dir = os.path.join(output_dir, f'arsr-{self.group_arsr}')
		out_path = utils.create_dirs(output_dir, self.logger.info)
		if not out_path.is_dir(): raise Exception('Invalid output directory!')
		path = out_path.joinpath(filename)

		# select function and params to open file
		if compress:
			open_func = gzip.open
			open_params = (path, 'at',)  # requires Python 3.6
		else:
			open_func = path.open
			open_params = ('a',)
		return open_func, open_params
