# Run this test with: python -m unittest
# from within the directory "evaluation".

import unittest

from . import utils, constants


class TestMeasurementUtils(unittest.TestCase):
	""" Contains some basic tests for measurement utils. """

	def test_convert_arsr_recommendation(self):
		""" Test conversion of ARSR results. """
		suggestion_example = {
			'S1': {
				'FAM1': {'SAM1': .3, 'SAM2': .4},
				'FAM2': {'SAM1': .1, 'SAM2': .2, 'SAM3': .9},
				'FAM3': {}  # will be discarded like SAM3
			},
			'S2': {
				'FAM1': {'SAM1': .1, 'SAM2': .2},
				'FAM2': {'SAM1': .3, 'SAM2': .4}
			},
			'S3': {}  # will be discarded
		}

		expected = {
			('FAM1', 'SAM1'): [('S1', .3), ('S2', .1)],
			('FAM1', 'SAM2'): [('S1', .4), ('S2', .2)],
			('FAM2', 'SAM1'): [('S2', .3), ('S1', .1)],
			('FAM2', 'SAM2'): [('S2', .4), ('S1', .2)],
		}

		recommendations = utils.convert_arsr_recommendation(
			suggestion_example,
			['FAM1', 'FAM2'],
			['SAM1', 'SAM2']
		)

		idx = 0
		for combi, r in recommendations.items():
			ex = expected[combi]
			self.assertEqual(len(r), 2, f'Items: {r}')
			self.assertEqual(r[0]['value'], ex[0][0], f'Wrong value for {combi} item 1')
			self.assertEqual(r[0]['score'], ex[0][1], f'Wrong score for {combi} item 1')
			self.assertEqual(r[1]['value'], ex[1][0], f'Wrong score for {combi} item 2')
			self.assertEqual(r[1]['score'], ex[1][1], f'Wrong score for {combi} item 2')
			idx += 1

	def test_count_same_score(self):
		""" Test counting how many recommendations have same score. """
		scores = [1, 2, 3, 4, 5]
		self.assertEqual(utils.count_same_score(scores), 0)
		scores = [1, 1, 2, 3, 4, 5]
		self.assertEqual(utils.count_same_score(scores), 2)
		scores = [1, 1, 2, 3, 4, 5, 5]
		self.assertEqual(utils.count_same_score(scores), 4)
		scores = [1, 1, 2, 3, 3, 3, 3, 4, 5, 5]
		self.assertEqual(utils.count_same_score(scores), 8)

	def test_compress_uri(self):
		""" Test compressing suggestion uris (by using prefix). """
		prefixes = {
			'http://purl.obolibrary.org/obo/': 'obo',
			'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#': 'ncit'
		}
		prefix_separator = ':'

		uri = 'http://purl.obolibrary.org/obo/BTO_0000938'
		expected = f'obo:BTO_0000938'
		result = utils.compress_uri(uri, prefixes, prefix_separator)
		self.assertEqual(result, expected)

		uri = 'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#C2866'
		expected = f'ncit:C2866'
		result = utils.compress_uri(uri, prefixes, prefix_separator)
		self.assertEqual(result, expected)

		# a value that is no URI
		uri = 'Some Example Value'
		expected = uri
		result = utils.compress_uri(uri, prefixes, prefix_separator)
		self.assertEqual(result, expected)

		# check list compression
		uris = [
			'http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#C2866',
			'http://purl.obolibrary.org/obo/BTO_0000938'
		]
		expected = ['ncit:C2866', 'obo:BTO_0000938']
		result = utils.compress_uri(uris, prefixes, prefix_separator)
		self.assertEqual(result, expected)

	def test_unique_before(self):
		""" Test unique_before function to get number of unique items before the index. """
		mappings = {
			'A': ['A1', 'A2', 'A3'],
			'B': ['B1', 'B2'],
			'B3': ['B']
		}

		# all items before are unique, assume 'C' as item of interest
		recommendations = ['A', 'B', 'C']
		unique_before = utils.unique_before(recommendations, 2, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 2)  # A and B are unique

		# test some are not unique, again assuming 'C' as ioi
		recommendations = ['A', 'B', 'B1', 'C']
		unique_before = utils.unique_before(recommendations, 3, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 2)  # A and B are unique
		self.assertEqual(len(recommendations), 4)  # no changes should be made to original list
		recommendations = ['A', 'A1', 'B', 'B1', 'C']
		unique_before = utils.unique_before(recommendations, 4, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 2)  # A and B are unique
		self.assertEqual(len(recommendations), 5)

		# test one that is not unique at end of list
		recommendations = ['A', 'A1', 'B', 'B1', 'C', 'D']
		unique_before = utils.unique_before(recommendations, 5, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 3)  # A, B, C are unique
		self.assertEqual(len(recommendations), 6)

		# test only one unique
		recommendations = ['A', 'A1', 'A2', 'A3', 'C']
		unique_before = utils.unique_before(recommendations, 4, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 1)  # A is unique
		self.assertEqual(len(recommendations), 5)

		# test no items left
		unique_before = utils.unique_before([], 0, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 0)

		# test single item
		unique_before = utils.unique_before(['A'], 1, mappings, enforce_uris=False)
		self.assertEqual(unique_before, 1)
