import logging
import sys


def configure_logging(filepath=None, debug=False, file_debug=True):
	""" Configures logging and returns the logger to use.

	Parameters
	----------
	filepath : str | None
	debug : bool
	file_debug : bool
	"""

	# configure logging
	# logging.basicConfig(level=logging.DEBUG)
	loglevel = logging.DEBUG if debug else logging.INFO
	root_logger = logging.getLogger()  # configure root logger
	root_logger.setLevel(loglevel)

	# console handler
	ch = logging.StreamHandler(sys.stdout)
	ch.setLevel(loglevel)

	# output formatter
	formatter = logging.Formatter('[%(levelname)s] %(asctime)s: %(message)s')
	ch.setFormatter(formatter)
	root_logger.addHandler(ch)

	if filepath is not None:
		fh = logging.FileHandler(filepath)
		fh.setLevel(logging.DEBUG if file_debug else logging.INFO)
		file_formatter = logging.Formatter('[%(levelname)s] %(asctime)s - %(name)s: %(message)s')
		fh.setFormatter(file_formatter)
		root_logger.addHandler(fh)

	return logging.getLogger(__name__)
